-- MySQL dump 10.13  Distrib 5.5.40, for Linux (x86_64)
--
-- Host: localhost    Database: SWS901
-- ------------------------------------------------------
-- Server version	5.5.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `SWS901`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `SWS901` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `SWS901`;

--
-- Table structure for table `gamehistory`
--

DROP TABLE IF EXISTS `gamehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamehistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recommendid` int(11) DEFAULT NULL,
  `ispass` tinyint(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamehistory`
--

LOCK TABLES `gamehistory` WRITE;
/*!40000 ALTER TABLE `gamehistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamehistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientid` int(11) DEFAULT NULL,
  `bodytemperature` float(4,2) DEFAULT NULL,
  `heartrate` smallint(6) DEFAULT NULL,
  `systolic` smallint(6) DEFAULT NULL,
  `diastolic` smallint(6) DEFAULT NULL,
  `respiratory` tinyint(4) DEFAULT NULL,
  `recdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1004 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES (1003,11,36.50,102,147,82,10,'2014-11-22 20:00:38');
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientid` int(11) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `recdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientid` int(11) DEFAULT NULL,
  `emergency_phone` varchar(16) DEFAULT NULL,
  `emergency_email` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (101,11,'123000','sfhf@fgj.com'),(102,14,'6474556350','j.j@yahoo.com'),(103,15,'6473588909','gdabice@gmail.com'),(104,16,'34584','vhs@hga.com'),(105,17,'0154','hshd@lil.com'),(106,18,'31578818','bshdhd@fiir.com'),(107,20,'',''),(108,21,'','');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommend`
--

DROP TABLE IF EXISTS `recommend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientid` int(11) DEFAULT NULL,
  `iswrist` tinyint(4) DEFAULT NULL,
  `message` varchar(256) DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `setdate` datetime DEFAULT NULL,
  `duedate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommend`
--

LOCK TABLES `recommend` WRITE;
/*!40000 ALTER TABLE `recommend` DISABLE KEYS */;
INSERT INTO `recommend` VALUES (1,11,0,'test your fatigue',1,2,'2014-11-21 14:55:19','2014-11-22 00:00:00');
/*!40000 ALTER TABLE `recommend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `lastname` varchar(32) DEFAULT NULL,
  `firstname` varchar(32) DEFAULT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `recdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (11,'ptest','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','last','p1','123456','wfg@fgh.com','123addr','2014-11-10 21:53:36'),(12,'ntest','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','se','nurs','000785','gahhs@shjd.com','112street','2014-11-10 21:55:24'),(13,'ntest2','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','de','nr','085','dhg@dyh.com','123cvhf','2014-11-10 21:58:04'),(14,'testpatient','b1b3773a05c0ed0176787a4f1574ff0075f7521e','Jose','Jackie','6473569987','jackie.jose@google.com','1028 Progress Ave. Toronto','2014-11-11 14:56:42'),(15,'ws.sim','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Sim','Victor','6472350909','victor.ws.sim@gmail.com','802 Sheppard Avenue East','2014-11-11 14:59:44'),(16,'victor','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Sim','WS','1234567890','qhdhfuhe@google.com','18383 Antr Drive','2014-11-11 15:01:57'),(17,'patient','403926033d001b5279df37cbbe5287b7c7c267fa','hahhahhah','lol','1234567890','lol@lil.com','no where','2014-11-11 15:03:20'),(18,'patient007','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Bonds','James','1234567890','shdur@fkf.com','134 didb street','2014-11-11 15:04:33'),(19,'','da39a3ee5e6b4b0d3255bfef95601890afd80709','','','','','','2014-11-16 11:04:31'),(20,'','da39a3ee5e6b4b0d3255bfef95601890afd80709','','','','','','2014-11-16 11:04:50'),(21,'','da39a3ee5e6b4b0d3255bfef95601890afd80709','','','','','','2014-11-16 11:04:58'),(22,'n','d1854cae891ec7b29161ccaf79a24b00c274bdaa','nl','nf','1234567898','cx@gmail.com','67 s f sq','2014-11-16 16:49:45');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-23  8:34:01
