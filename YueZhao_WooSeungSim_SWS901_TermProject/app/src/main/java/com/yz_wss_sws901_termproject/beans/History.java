package com.yz_wss_sws901_termproject.beans;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

public class History  implements Serializable, Parcelable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5243770848230367232L;
	private String patientId;
	private String bodyTemp;
	private String heartrate;
	private String systolic;
	private String diastolic;
	private String respiratory;
	private String recDate;
	
	public History(){
		
	}
	public History(String patientId, String bodyTemp, String heartrate,
			String systolic, String diastolic, String respiratory){
		this.patientId = patientId;
		this.bodyTemp = bodyTemp;
		this.heartrate = heartrate;
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.respiratory = respiratory;
	}
	public History(String patientId, String bodyTemp, String heartrate,
			String systolic, String diastolic, String respiratory, String recDate){
		this.patientId = patientId;
		this.bodyTemp = bodyTemp;
		this.heartrate = heartrate;
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.respiratory = respiratory;
		setRecDate(recDate);
	}
	public History(String patientId, String bodyTemp, String heartrate,
			String systolic, String diastolic, String respiratory, Date recDate){
		this.patientId = patientId;
		this.bodyTemp = bodyTemp;
		this.heartrate = heartrate;
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.respiratory = respiratory;
		setRecDate(recDate);
	}
	
	public Date getRecDatebyFormat(){
		Date tempDate=null;
		try {
			tempDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).parse(this.recDate);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return tempDate;
	}
	public void setRecDate(String recDate){
		this.recDate = recDate;
	}
	public void setRecDate(Date recDate){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		this.recDate = sdf.format(recDate);
	}
	public void setRespiratory(String respiratory){
		this.respiratory = respiratory;
	}
	public void setDiastolic(String diastolic){
		this.diastolic = diastolic;
	}
	public void setSystolic(String systolic){
		this.systolic = systolic;
	}
	public void setHeartrate(String heartrate){
		this.heartrate = heartrate;
	}
	public void setBodyTemp(String bodyTemp){
		this.bodyTemp = bodyTemp;
	}
	public void setPatientId(String patientId){
		this.patientId = patientId;
	}
	
	public String getPatientId(){
		return this.patientId;
	}
	public String getBodyTemp(){
		return this.bodyTemp;
	}
	public String getHeartrate(){
		return this.heartrate;
	}
	public String getSystolic(){
		return this.systolic;
	}
	public String getDiastolic(){
		return this.diastolic;
	}
	public String getRespiratory(){
		return this.respiratory;
	}
	public String getRecDate(){
		return this.recDate;
	}
	

	
	@Override
	public int describeContents() {
		return 0;
	}
	/*
	private String patientId;
	private String nurseId;
	private String bodyTemp;
	private String heartrate;
	private String systolic;
	private String diastolic;
	private String respiratory;
	private String recDate;
 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getPatientId());
		dest.writeString(getBodyTemp());
		dest.writeString(getHeartrate());
		dest.writeString(getSystolic());
		dest.writeString(getDiastolic());
		dest.writeString(getRespiratory());
		dest.writeString(getRecDate());
		
	}
	
	public History(Parcel source){
		setPatientId(source.readString());
		setBodyTemp(source.readString());
		setHeartrate(source.readString());
		setSystolic(source.readString());
		setDiastolic(source.readString());
		setRespiratory(source.readString());
		setRecDate(source.readString());
	}
	
	public static final Creator<History> CREATOR =
			new Creator<History>(){

				@Override
				public History createFromParcel(Parcel source) {
					Log.i("History", "createFromParcel()");
					return new History(source);
				}

				@Override
				public History[] newArray(int size) {
					Log.i("History", "newArray()");
					return new History[size];
				}
		
		};
	

}
