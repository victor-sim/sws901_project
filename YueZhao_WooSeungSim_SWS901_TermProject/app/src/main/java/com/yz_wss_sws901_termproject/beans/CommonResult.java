package com.yz_wss_sws901_termproject.beans;

import java.io.Serializable;

public class CommonResult implements Serializable{
	
	private String result;
	
	public CommonResult() {
	}
	
	private static final long serialVersionUID = 1L;
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
}