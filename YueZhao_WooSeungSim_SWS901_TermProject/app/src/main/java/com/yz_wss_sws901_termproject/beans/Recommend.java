package com.yz_wss_sws901_termproject.beans;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;


/**
 * Created by VictorSim on 14-11-21.
 */
public class Recommend   implements Serializable, Parcelable{

    private String patientId;
    private boolean isWrist;
    private String message;
    private int level;
    private int duration;
    private String setDate;
    private String dueDate;


    public Recommend(){

    }

    public Recommend(String patientId, boolean isWrist, String message, int level, int duration, String setDate, String dueDate){
        setPatientId(patientId);
        setIsWrist(isWrist);
        setMessage(message);
        setLevel(level);
        setDuration(duration);
        setSetDate(setDate);
        setDueDate(dueDate);
    }

    public Recommend(String patientId, boolean isWrist, String message, int level, int duration, Date setDate, Date dueDate){
        setPatientId(patientId);
        setIsWrist(isWrist);
        setMessage(message);
        setLevel(level);
        setDuration(duration);
        setSetDate(setDate);
        setDueDate(dueDate);
    }

    public Recommend(String patientId, boolean isWrist, String message, int level, int duration, String dueDate){
        setPatientId(patientId);
        setIsWrist(isWrist);
        setMessage(message);
        setLevel(level);
        setDuration(duration);
        setDueDate(dueDate);
    }

    public Recommend(String patientId, boolean isWrist, String message, int level, int duration, Date dueDate){
        setPatientId(patientId);
        setIsWrist(isWrist);
        setMessage(message);
        setLevel(level);
        setDuration(duration);
        setDueDate(dueDate);
    }


    public int getDuration(){
        return duration;
    }
    public void setDuration(int duration){
        this.duration = duration;
    }
    public void setLevel(int level){
        this.level = level;
    }
    public int getLevel(){
        return level;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return message;
    }
    public void setIsWrist(boolean isWrist){
        this.isWrist = isWrist;
    }
    public boolean getIsWrist(){
        return isWrist;
    }
    public String getPatientId(){
        return patientId;
    }
    public void setPatientId(String patientId){
        this.patientId = patientId;
    }

    public String getDueDate(){
        return dueDate;
    }
    public String getSetDate(){
        return setDate;
    }
    public Date getDueDatebyFormat(){
        Date tempDate=null;
        try {
            tempDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(this.dueDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tempDate;
    }
    public Date getSetDatebyFormat(){
        Date tempDate=null;
        try {
            tempDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(this.setDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tempDate;
    }
    public void setSetDate(String setDate){
        this.setDate = setDate;
    }
    public void setSetDate(Date setDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        this.setDate = sdf.format(setDate);
    }
    public void setDueDate(String dueDate){
        this.dueDate = dueDate;
    }
    public void setDueDate(Date dueDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        this.dueDate = sdf.format(dueDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getPatientId());
        dest.writeString(Boolean.toString(getIsWrist()));
        dest.writeString(getMessage());
        dest.writeInt(getLevel());
        dest.writeInt(getDuration());
        dest.writeString(getSetDate());
        dest.writeString(getDueDate());

    }

    public Recommend(Parcel source){
        setPatientId(source.readString());
        String isWrist = source.readString();
        setIsWrist(Boolean.parseBoolean(isWrist));
        setMessage(source.readString());
        setLevel(source.readInt());
        setDuration(source.readInt());
        setSetDate(source.readString());
        setDueDate(source.readString());
    }

    public static final Creator<Recommend> CREATOR =
            new Creator<Recommend>(){

                @Override
                public Recommend createFromParcel(Parcel source) {
                    Log.i("Recommend", "createFromParcel()");
                    return new Recommend(source);
                }

                @Override
                public Recommend[] newArray(int size) {
                    Log.i("Recommend", "newArray()");
                    return new Recommend[size];
                }

            };

    @Override
    public String toString(){
        return "Recommend:"+this.getPatientId()+", "+this.getIsWrist()+",\n "+this.getMessage()
                +",\n"+this.getLevel()+", "+this.getDuration()
                +",\n"+this.getSetDate()+"\n"
                +this.getDueDate();
    }

}
