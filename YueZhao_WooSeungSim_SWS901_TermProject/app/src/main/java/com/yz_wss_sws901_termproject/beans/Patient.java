package com.yz_wss_sws901_termproject.beans;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;

public class Patient extends User  implements Serializable, Parcelable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4425760083141813152L;
	private String emergency_phone;
	private String emergency_email;
	
	public Patient(){
		super();
	}
	
	public Patient(String id, String username, String password, String firstname,
			String lastname, String telephone, String email, 
			String address, String emer_phone, String emer_email) {
		super(id, username, password, firstname, lastname, telephone, email, address, true);
		this.emergency_phone = emer_phone;
		this.emergency_email = emer_email;
	}
	
	public Patient(String username, String password, String firstname,
			String lastname, String telephone, String email, 
			String address, String emer_phone, String emer_email) {
		super(username, password, firstname, lastname, telephone, email, address, true);
		this.emergency_phone = emer_phone;
		this.emergency_email = emer_email;
	}

    public Patient(String id, String username, String password, String firstname,
                   String lastname, String telephone, String email,
                   String address, boolean isPatient, String recDate, String emer_phone, String emer_email) {
        super(id, username, password, firstname, lastname, telephone, email, address, isPatient, recDate);
        this.emergency_phone = emer_phone;
        this.emergency_email = emer_email;
    }

    public Patient(String username, String password, String firstname,
                   String lastname, String telephone, String email,
                   String address, boolean isPatient, String recDate, String emer_phone, String emer_email) {
        super(username, password, firstname, lastname, telephone, email, address, isPatient, recDate);
        this.emergency_phone = emer_phone;
        this.emergency_email = emer_email;
    }

    @Override
    public String toString(){
        return "Patient:"+this.getUsername()+", "+this.getFirstname()+", "+this.getLastname()
                +"\n"+this.getEmail()+", "+this.getTelephone()+", "+this.getAddress()+"\n"
                +this.getRecDatebyFormatString() + "\n"
                +this.getEmer_email()+", "+this.getEmer_phone();
    }
	
	public String getEmer_phone(){
		return emergency_phone;
	}
	public String getEmer_email(){
		return emergency_email;
	}
	public void setEmer_phone(String emer_phone){
		this.emergency_phone = emer_phone;
	}
	public void setEmer_email(String emer_email){
		this.emergency_email = emer_email;
	}
	
	/*
	    private String id;
		private String username;
		private String password;
		private String firstname;
		private String lastname;
		private String telephone;
		private String email;
		private String address;
		private boolean isPatient;
		private String recDate;
		private String emer_phone;
		private String emer_email;
	*/
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getId());
		dest.writeString(getUsername());
		dest.writeString(getPassword());
		dest.writeString(getFirstname());
		dest.writeString(getLastname());
		dest.writeString(getTelephone());
		dest.writeString(getEmail());
		dest.writeString(getAddress());
		dest.writeString(Boolean.toString(isPatient()));
		dest.writeString(getRecDate());
		dest.writeString(getEmer_phone());
		dest.writeString(getEmer_email());
	}
	
	public Patient(Parcel source){
		setId(source.readString());
		setUsername(source.readString());
		setPassword(source.readString());
		setFirstname(source.readString());
		setLastname(source.readString());
		setTelephone(source.readString());
		setEmail(source.readString());
		setAddress(source.readString());
		String isPatient = source.readString();
		setIsPatient(Boolean.parseBoolean(isPatient));
		setRecDate(source.readString());
		setEmer_phone(source.readString());
		setEmer_email(source.readString());
	}

	public static final Creator<Patient> CREATOR =
			new Creator<Patient>(){
	
				@Override
				public Patient createFromParcel(Parcel source) {
					Log.i("Patient", "createFromParcel()");
					return new Patient(source);
				}
	
				@Override
				public Patient[] newArray(int size) {
					Log.i("Patient", "newArray()");
					return new Patient[size];
				}
		
		};

}
