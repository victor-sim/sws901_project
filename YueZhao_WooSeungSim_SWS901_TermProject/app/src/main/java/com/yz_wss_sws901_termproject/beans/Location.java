package com.yz_wss_sws901_termproject.beans;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by VictorSim on 14-11-24.
 */
public class Location   implements Serializable, Parcelable {
    private String patientid;
    private double latitude;
    private double longitude;
    private String recdate;

    public Location(){
        super();
    }

    public Location(String patientid, double latitude, double longitude){
        this.patientid = patientid;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public Location(String patientid, double latitude, double longitude, String recdate){
        this.patientid = patientid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.recdate = recdate;
    }
    public Location(String patientid, double latitude, double longitude, Date recdate){
        this.patientid = patientid;
        this.latitude = latitude;
        this.longitude = longitude;
        setRecDate(recdate);
    }

    public String getPatientid(){
        return this.patientid;
    }
    public void setPatientid(String patientid){
        this.patientid=patientid;
    }
    public double getLatitude(){
        return this.latitude;
    }
    public void setLatitude(double latitude){
        this.latitude = latitude;
    }
    public double getLongitude(){
        return this.longitude;
    }
    public void setLongitude(double longitude){
        this.longitude = longitude;
    }

    public Date getRecDatebyFormat(){
        Date tempDate=null;
        try {
            tempDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).parse(this.recdate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tempDate;
    }
    public String getRecdate(){
        return this.recdate;
    }
    public void setRecDate(String recDate){
        this.recdate = recDate;
    }
    public void setRecDate(Date recDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        this.recdate = sdf.format(recDate);
    }

    @Override
    public String toString(){
        return "Patient:"+this.getPatientid()
                +"\n"+this.getLatitude()+", "+this.getLongitude()+"\n"
                +this.getRecDatebyFormat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getPatientid());
        dest.writeDouble(getLatitude());
        dest.writeDouble(getLongitude());
        dest.writeString(getRecdate());
    }

    public Location(Parcel source){
        setPatientid(source.readString());
        setLatitude(source.readDouble());
        setLongitude(source.readDouble());
        setRecDate(source.readString());
    }

    public static final Creator<Location> CREATOR =
            new Creator<Location>(){

                @Override
                public Location createFromParcel(Parcel source) {
                    Log.i("Location", "createFromParcel()");
                    return new Location(source);
                }

                @Override
                public Location[] newArray(int size) {
                    Log.i("Location", "newArray()");
                    return new Location[size];
                }

            };
}
