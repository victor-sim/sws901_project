package com.yz_wss_sws901_termproject.beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

public class User  implements Serializable, Parcelable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -260367191170354469L;
	private String id;
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String telephone;
	private String email;
	private String address;
	private boolean isPatient;
	private String recDate;
	
	public User(){
		
	}
	
	public User(String id, String username, String password, 
			String firstname, String lastname, String telephone, 
			String email, String address, boolean isPatient){
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.telephone = telephone;
		this.email = email;
		this.address = address;
		this.isPatient = isPatient;
	}
	
	public User(String username, String password, 
			String firstname, String lastname, String telephone,
			String email, String address, boolean isPatient){
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.telephone = telephone;
		this.email = email;
		this.address = address;
		this.isPatient = isPatient;
	}

    public User(String id, String username, String password,
                String firstname, String lastname, String telephone,
                String email, String address, boolean isPatient, String recDate){
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.telephone = telephone;
        this.email = email;
        this.address = address;
        this.isPatient = isPatient;
        this.recDate = recDate;
    }

    public User(String username, String password,
                String firstname, String lastname, String telephone,
                String email, String address, boolean isPatient, String recDate){
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.telephone = telephone;
        this.email = email;
        this.address = address;
        this.isPatient = isPatient;
        this.recDate = recDate;
    }

	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
	public void setRecDate(String recDate){
		this.recDate = recDate;
	}
	public void setRecDate(Date recDate){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		this.recDate = sdf.format(recDate);
	}
	public String getRecDate(){
		return this.recDate;
	}
	public Date getRecDatebyFormat(){
		Date tempDate=null;
		try {
			tempDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).parse(this.recDate);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return tempDate;
	}
	public String getRecDatebyFormatString(){
		Date tempDate=null;
		try {
			tempDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).parse(this.recDate);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		return sdf.format(tempDate);
	}
	public void setUsername(String username){
		this.username = username;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setFirstname(String firstname){
		this.firstname = firstname;
	}
	public void setLastname(String lastname){
		this.lastname = lastname;
	}
	public void setTelephone(String telephone){
		this.telephone = telephone;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public void setAddress(String address){
		this.address = address;
	}
	public void setIsPatient(boolean isPatient){
		this.isPatient = isPatient;
	}
	
	public String getUsername(){
		return this.username;
	}
	public String getPassword(){
		return this.password;
	}
	public String getFirstname(){
		return this.firstname;
	}
	public String getLastname(){
		return this.lastname;
	}
	public String getTelephone(){
		return this.telephone;
	}
	public String getEmail(){
		return this.email;
	}
	public String getAddress(){
		return this.address;
	}
	public boolean isPatient(){
		return this.isPatient;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	/*
	    private String id;
		private String username;
		private String password;
		private String firstname;
		private String lastname;
		private String telephone;
		private String email;
		private String address;
		private boolean isPatient;
		private String recDate;
	*/
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getId());
		dest.writeString(getUsername());
		dest.writeString(getPassword());
		dest.writeString(getFirstname());
		dest.writeString(getLastname());
		dest.writeString(getTelephone());
		dest.writeString(getEmail());
		dest.writeString(getAddress());
		dest.writeString(Boolean.toString(isPatient()));
		dest.writeString(getRecDate());
	}
	
	public User(Parcel source){
		setId(source.readString());
		setUsername(source.readString());
		setPassword(source.readString());
		setFirstname(source.readString());
		setLastname(source.readString());
		setTelephone(source.readString());
		setEmail(source.readString());
		setAddress(source.readString());
		String isPatient = source.readString();
		setIsPatient(Boolean.parseBoolean(isPatient));
		setRecDate(source.readString());
	}
	
	public static final Creator<User> CREATOR =
			new Creator<User>(){

				@Override
				public User createFromParcel(Parcel source) {
					Log.i("User", "createFromParcel()");
					return new User(source);
				}

				@Override
				public User[] newArray(int size) {
					Log.i("User", "newArray()");
					return new User[size];
				}
		
		};

}
