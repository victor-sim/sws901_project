package com.yz_wss_sws901_termproject.service;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
//import android.widget.Toast;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.tools.GPSTracker;
import com.yz_wss_sws901_termproject.web.*;
import com.yz_wss_sws901_termproject.beans.*;

/**
 * Created by VictorSim on 14-11-23.
 */
public class LocationReportingService extends Service{

    // when client sent new order to server
    final public static int NEW_ORDER = 0;

    // when client get order confirm message from server
    // start this service to check when order is completed
    final public int PROCESSING_ORDER = 1;

    private int reason=0;
    private Timer updateTimer;
    private boolean autoUpdate = true;
    private int updateFreq;

    private NotificationManager notificationManager;
    private Notification.Builder foodNotiBuilder;
    private int NOTIFICATION_ID = 0;

    private GPSTracker oGps;
    private Context mContext;

    private String patientId;

    private SharedPreferences app_preferences;


    private int selectionToSecond(int position){
        int freqSecond = 0;
        switch(position){
            case 0:
                freqSecond = 10;
                break;
            case 1:
                freqSecond = 30;
                break;
            case 2:
                freqSecond = 60;
                break;
            case 3:
                freqSecond = 300;
                break;
            case 4:
                freqSecond = 600;
                break;
            case 5:
                freqSecond = 900;
                break;
            case 6:
                freqSecond = 1800;
                break;
            default:
                freqSecond = 3600;
                break;
        }
        return freqSecond;
    }

    @Override
    public void onCreate() {

        mContext = this;
        updateTimer = new Timer("LocationReporter");

        notificationManager
                = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        foodNotiBuilder = new Notification.Builder(this);

        //Toast.makeTextToast.makeText(mContext, "LocationReporter onCreate()", Toast.LENGTH_LONG).show();
        oGps = new GPSTracker(mContext);

        if(app_preferences == null){
            String sharedPref = getString(R.string.sharedPref);
            app_preferences = getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        }
        int position = app_preferences.getInt("reportingPeriod", 7);

        Log.e("Service onCreate", "reportingPeriod " + position);
        updateFreq = selectionToSecond(position);

    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e("Loc update Service", "onBind()");
        return null;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        Log.e("Loc update Service", "onDestroy()");
        updateTimer.cancel();
        // Tell the user we stopped.
        //Toast.makeText(this, "Service Stoppted", Toast.LENGTH_SHORT).show();


        if(app_preferences == null){
            String sharedPref = getString(R.string.sharedPref);
            app_preferences = getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        }
        boolean isChecked = app_preferences.getBoolean("locationReporting", false);
        if(isChecked)
            sendBroadcast(new Intent("SWS901_HomeDoctor_Restart"));
        //SWS901_HomeDoctor_Restart
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private boolean startTimer(){
        if(app_preferences == null){
            String sharedPref = getString(R.string.sharedPref);
            app_preferences = getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        }
        int position = app_preferences.getInt("reportingPeriod", 7);
        switch(position){
            case 0:
                updateFreq = 10;
                break;
            case 1:
                updateFreq = 30;
                break;
            case 2:
                updateFreq = 60;
                break;
            case 3:
                updateFreq = 300;
                break;
            case 4:
                updateFreq = 600;
                break;
            case 5:
                updateFreq = 900;
                break;
            case 6:
                updateFreq = 1800;
                break;
            default:
                updateFreq = 3600;
                break;
        }
        boolean isChecked = app_preferences.getBoolean("locationReporting", false);
        patientId = app_preferences.getString("patientId", null);
        if(patientId == null){
            updateTimer.cancel();
            return false;
        }
        return true;
    }

    public void restartTimer(){
        if(startTimer()){
            NOTIFICATION_ID++;
            if(isNetworkAvailable()){
                updateTimer.cancel();

                if(autoUpdate) {
                    updateTimer = new Timer("LocationReporter");
                    long time = System.currentTimeMillis();
                    long prevTime = app_preferences.getLong("lastReport", time-updateFreq*1000);
                    long timeGap = time - prevTime;

                    if(timeGap < updateFreq*1000){
                        timeGap = updateFreq*1000 - timeGap;
                    }else{
                        timeGap = 1000;
                    }

                    updateTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            refreshLocation();
                        }
                    }
                            , timeGap, updateFreq*1000);

                }
            }else{
                updateTimer.cancel();
                if(autoUpdate) {
                    updateTimer = new Timer("LocationReporter");
                    updateTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            restartTimer();
                        }
                    }
                            , 1000, 10000);

                }
            }
        }else{
            updateTimer.cancel();
            if(autoUpdate) {
                updateTimer = new Timer("LocationReporter");
                updateTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        restartTimer();
                    }
                }
                        , 1000, 120000);

            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(startTimer()){
            NOTIFICATION_ID++;
            if(isNetworkAvailable()){
                updateTimer.cancel();
                if(autoUpdate) {
                    updateTimer = new Timer("LocationReporter");
                    long time = System.currentTimeMillis();
                    long prevTime = app_preferences.getLong("lastReport", time-updateFreq*1000);
                    long timeGap = time - prevTime;

                    if(timeGap < updateFreq*1000){
                        timeGap = updateFreq*1000 - timeGap;
                    }else{
                        timeGap = 1000;
                    }
                    updateTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            refreshLocation();
                        }
                    }
                            , timeGap, updateFreq*1000);

                }
            }else{
                updateTimer.cancel();
                if(autoUpdate) {
                    updateTimer = new Timer("LocationReporter");
                    updateTimer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            restartTimer();
                        }
                    }
                            , 1000, 10000);

                }
            }
        }else{
            updateTimer.cancel();
            if(autoUpdate) {
                updateTimer = new Timer("LocationReporter");
                updateTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        restartTimer();
                    }
                }
                        , 1000, 120000);

            }
        }

        //refreshStatus();
        return Service.START_REDELIVER_INTENT;
    }

    private String refreshLocation(){

        Thread thread = new Thread() {

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void run() {
                super.run();
                WebRequest request = new WebRequest(mContext);
                String result = null;

                try {

                    oGps.getLocation();
                    result = request.addLocation(patientId, oGps.getLongitude(), oGps.getLatitude());
                    CommonResult resultCom = ParseData.CommonPares(result);
                    String status =  resultCom.getResult();

                    String notiMsg = null;
                    String tickerMsg = "Your location information was updated";
                    notiMsg = "Your location information was updated successfuly";

                    long time = System.currentTimeMillis();

                    SharedPreferences.Editor editor = app_preferences.edit();
                    editor.remove("lastReport");
                    editor.putLong("lastReport", time);
                    editor.apply();
                    editor.commit();

                    //Notification Bar
                    foodNotiBuilder
                            .setTicker(tickerMsg)
                            .setContentTitle("Location update")
                            .setContentText(notiMsg)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setShowWhen(true)
                            .setWhen(time);

                    if (android.os.Build.VERSION.SDK_INT>=android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        // call something for API Level 16+
                        notificationManager.notify(NOTIFICATION_ID,
                                foodNotiBuilder.build() );
                    } else if (android.os.Build.VERSION.SDK_INT>=android.os.Build.VERSION_CODES.HONEYCOMB) {
                        // call something for API Level 11+
                        notificationManager.notify(NOTIFICATION_ID,
                                foodNotiBuilder.getNotification() );
                    }
                }/* catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
                catch(Exception e){
                    e.printStackTrace();
                }
                //stopSelf();
            }
        };
        thread.start();


        return null;
    }

}
