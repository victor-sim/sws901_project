package com.yz_wss_sws901_termproject.service;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.yz_wss_sws901_termproject.R;

public class ReportingBroadcastReceiver extends BroadcastReceiver {
    private SharedPreferences app_preferences;
    public ReportingBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String sharedPref = context.getResources().getString(R.string.sharedPref);
        app_preferences = context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE);

        //Toast.makeText(context, "Broadcast Recv: " +intent.getAction(), Toast.LENGTH_LONG).show();

        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            ComponentName cName = new ComponentName(context.getPackageName(), LocationReportingService.class.getName());
            ComponentName svcName = context.startService(new Intent().setComponent(cName));

            if(svcName == null) {
                Log.d("Broadcast Receiver", "Error");
                Toast.makeText(context, "Location reporting error", Toast.LENGTH_LONG).show();

            }  else {
                Log.d("Broadcast Receiver", "Done");
                Toast.makeText(context, "Location reporting started", Toast.LENGTH_LONG).show();
            }
        } else if(intent.getAction().equals("SWS901_HomeDoctor_Restart")){
            ComponentName cName = new ComponentName(context.getPackageName(), LocationReportingService.class.getName());
            ComponentName svcName = context.startService(new Intent().setComponent(cName));

            if(svcName == null) {
                Log.d("Broadcast Receiver", "Error");
                Toast.makeText(context, "Location reporting error", Toast.LENGTH_LONG).show();

            }  else {
                Log.d("Broadcast Receiver", "Done");
                Toast.makeText(context, "Location reporting restarted", Toast.LENGTH_LONG).show();
            }
        }
    }
}
