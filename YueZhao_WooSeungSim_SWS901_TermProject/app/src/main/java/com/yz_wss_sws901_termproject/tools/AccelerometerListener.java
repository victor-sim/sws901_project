package com.yz_wss_sws901_termproject.tools;

public interface AccelerometerListener {
    
    public void onAccelerationChanged(float x, float y, float z);
  
    public void onShake(float force);
    
    public void setProgress(float progress);
  
}