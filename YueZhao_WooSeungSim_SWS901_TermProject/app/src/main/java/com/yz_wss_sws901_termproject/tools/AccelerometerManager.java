package com.yz_wss_sws901_termproject.tools;

import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;

public class AccelerometerManager {
	
	private static Context mContext = null;
	
	private static float goal = 2000.0f;
	private static float threshold = 15.0f;
	private static int interval = 2000;
	
	private static Sensor oSensor;
	private static SensorManager mgrSensor;
	
	private static AccelerometerListener accListener;
	
	private static Boolean isSupported;
	private static boolean isRunning = false;
	
	/*
	 * if manager is listening to orientation changes, return true
	 */
	public static boolean isListening(){
		return isRunning;
	}
	
	/*
	 * Register listener and start with setting threshold and interval
	 */
	private static SensorEventListener sensorEvtListener = new SensorEventListener(){
			private long lastUpdate =0;
			private long lastShake = 0;
	        private float lastX = 0;
	        private float lastY = 0;
	        private float lastZ = 0;
	        private float force = 0;
	        private float accumulated = 0;
	        
	        public void onAccuracyChanged(Sensor sensor, int accuracy) {
	        	
	        }
	        public void onSensorChanged(SensorEvent event) {
	        	// use the event timestamp as reference, so the manager precision won't depends 
	            // on the AccelerometerListener implementation processing time
	            long now = event.timestamp;
	            float forceX = event.values[0];
	            float forceY = event.values[1];
	            float forceZ = event.values[2];
	        
	            if (lastUpdate == 0) {
	            	lastShake = lastUpdate = now;
	                lastX = forceX;
	                lastY = forceY;
	                lastZ = forceZ;
	                //Toast.makeText(mContext,"No Motion detected", Toast.LENGTH_SHORT).show();
	            }else {
	                long timeDiff = now - lastUpdate;
	                
	                if (timeDiff > 0) { 
	                     
	                    force = Math.abs(forceX + forceY + forceZ - lastX - lastY - lastZ);
	                    //force = Math.abs(forceX + y + z - lastX - lastY - lastZ);
	                    
	                    if (Float.compare(force, threshold) >0 ) {
	                    	accumulated += force;
	                    	accListener.setProgress(accumulated/goal);
	                    	
	                    	if(accumulated >= goal){
	                    		accListener.onShake(force);
	                        	lastUpdate = 0;
	                        	force = 0;
	                        	accumulated = 0;
	                    	}
	                    	
	                        //Toast.makeText(Accelerometer.getContext(), 
	                        //(now-lastShake)+"  >= "+interval, 1000).show();
	                        /*
	                        if (now - lastShake >= interval) { 
	                            // trigger shake event
	                        	accListener.onShake(force);
	                        	lastUpdate = 0;
	                        	force = 0;
	                        }else{
	                            Toast.makeText(mContext,"No Motion detected", Toast.LENGTH_SHORT).show();
	                        }*/
	                        lastShake = now;
	                    }
	                    lastX = forceX;
	                    lastY = forceY;
	                    lastZ = forceZ;
	                    lastUpdate = now; 
	                }else{
	                    //Toast.makeText(mContext,"No Motion detected", Toast.LENGTH_SHORT).show();
	                }
	            }
	            // trigger change event
	            accListener.onAccelerationChanged(forceX, forceY, forceZ);
	        }
	};
	
	
	/*
	 * Unregister listener
	 */
	public static void stopListening(){
		isRunning = false;
		try{
			if(mgrSensor != null && sensorEvtListener != null){
				mgrSensor.unregisterListener(sensorEvtListener);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*
	 * Returns true if at least one Accelerometer sensor is available
     */
	public static boolean isSupported(Context context) {
		mContext = context;
		if(isSupported == null){
			if(mContext != null){
				mgrSensor = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
				// Check sensors on device
				List<Sensor> lstSensors = mgrSensor.getSensorList(Sensor.TYPE_ACCELEROMETER);
				isSupported = new Boolean(lstSensors.size()>0);
			}else{
				isSupported = Boolean.FALSE;
			}
		}
		return isSupported;
	}
	
	/*
	 * Set listener threshold and interval
	 */
	public static void configure(float threshold, int interval) {
        AccelerometerManager.threshold = threshold;
        AccelerometerManager.interval = interval;
    }

    public static void setGoal(float goal){
        AccelerometerManager.goal = goal;
    }
	
	public static float getThreshold(){
		return AccelerometerManager.threshold;
	}
	
	public static int getInterval(){
		return AccelerometerManager.interval;
	}
	
	/*
	 * Register listener and start it
	 */
	public static void startListening( AccelerometerListener accelerometerListener ){
		mgrSensor = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
		
		// get all sensors
		List<Sensor> lstSensors = mgrSensor.getSensorList(Sensor.TYPE_ACCELEROMETER);
		if(lstSensors.size()>0){
			oSensor = lstSensors.get(0);
			isRunning = mgrSensor.registerListener(sensorEvtListener, oSensor, SensorManager.SENSOR_DELAY_GAME);
			accListener = accelerometerListener;
		}
	}
	
	

}
