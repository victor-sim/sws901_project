package com.yz_wss_sws901_termproject.ui.patient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.beans.Recommend;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;


public class GameFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    private TextView txtPrescriptionTitle;
    private TextView txtPrescriptionDesc;
    private Spinner spnKine;
    private TextView txtKineOwnSpin;
    private Spinner spnLevel;
    private TextView txtLevel;
    private TextView txtDuration;
    private Spinner spnDuration;

    private TextView txtMessage;

    private Patient oPatient;
    private Recommend oRecommend;

    private boolean isWrist = true;
    private int level=1;
    private int duration = 2000;

    public void runGame(){
        Intent intent = new Intent(getActivity() , GameActivity.class);

        if(oRecommend != null) {
            intent.putExtra("recommend", (Serializable) oRecommend);
        }else{
            intent.putExtra("isWrist", isWrist);
            intent.putExtra("level", level);
            intent.putExtra("duration", duration);
        }
        startActivity(intent);
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GameFragment.
     */
    public static GameFragment newInstance(String param1, String param2) {
        GameFragment fragment = new GameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public GameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);

        Bundle extras = getActivity().getIntent().getExtras();
        oPatient = extras.getParcelable("user");

        txtMessage = (TextView) rootView.findViewById(R.id.txtMessage);
        txtPrescriptionTitle = (TextView) rootView.findViewById(R.id.txtKineTitle);
        txtPrescriptionDesc = (TextView) rootView.findViewById(R.id.txtDesc);
        txtKineOwnSpin = (TextView) rootView.findViewById(R.id.txtKineOwnSpin);
        spnKine = (Spinner) rootView.findViewById(R.id.spnType);
        txtDuration = (TextView) rootView.findViewById(R.id.txtDuration);
        txtLevel = (TextView) rootView.findViewById(R.id.txtLevel);
        spnDuration = (Spinner) rootView.findViewById(R.id.spnDuration);
        spnLevel = (Spinner) rootView.findViewById(R.id.spnLevel);
        spnDuration.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener(){
                    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
                        switch(pos){
                            case 0:
                                duration = 1;
                                break;
                            case 1:
                                duration = 2;
                                break;
                            case 2:
                            default:
                                duration = 3;
                                break;
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        ;
                    }
                }
        );
        spnLevel.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener(){
                    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
                        switch(pos){
                            case 0:
                                level = 1;
                                break;
                            case 1:
                                level = 2;
                                break;
                            case 2:
                            default:
                                level = 3;
                                break;
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        ;
                    }
                }
        );

        spnKine.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener(){
                    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
                        switch(pos){
                            case 0:
                                txtPrescriptionDesc.setText(getResources().getString(R.string.wrist_ex));
                                isWrist = true;
                                break;
                            case 1:
                            default:
                                txtPrescriptionDesc.setText(getResources().getString(R.string.shoulder_ex));
                                isWrist = false;
                                break;
                        }
                        txtPrescriptionDesc.invalidate();
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        ;
                    }
                }
        );

        new GetData(rootView.getContext(), oPatient.getId()).execute("");

        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        super.onAttach(activity);
        ((PatientMainActivity) activity).onSectionAttached(3);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private class GetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;
        private String patientId;



        private GetData(Context context, String patientId) {
            this.mContext = context;
            this.patientId = patientId;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.getRecommend(Integer.parseInt(patientId));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {
                Toast.makeText(mContext, "no recommended exercises", Toast.LENGTH_LONG);
                txtPrescriptionTitle.setText(getString(R.string.kine_own));
                txtPrescriptionDesc.setText(getString(R.string.wrist_ex));
                txtKineOwnSpin.setVisibility(View.VISIBLE);
                txtLevel.setVisibility(View.VISIBLE);
                txtDuration.setVisibility(View.VISIBLE);
                spnKine.setEnabled(true);
                spnKine.setVisibility(View.VISIBLE);
                spnLevel.setVisibility(View.VISIBLE);
                spnDuration.setVisibility(View.VISIBLE);
                spnLevel.setEnabled(true);
                spnDuration.setEnabled(true);
            } else {

                Log.e("onPostExecute", result);
                try {
                    oRecommend = ParseData.parseRecommend(result);

                    if(oRecommend != null){
                        txtPrescriptionTitle.setText(getString(R.string.kine_presc));
                        if(oRecommend.getIsWrist())
                            txtPrescriptionDesc.setText(getString(R.string.wrist_ex));
                        else
                            txtPrescriptionDesc.setText(getString(R.string.shoulder_ex));
                        txtKineOwnSpin.setVisibility(View.GONE);
                        txtLevel.setVisibility(View.GONE);
                        txtDuration.setVisibility(View.GONE);
                        spnKine.setEnabled(false);
                        spnDuration.setEnabled(false);
                        spnLevel.setEnabled(false);
                        spnKine.setVisibility(View.GONE);
                        spnDuration.setVisibility(View.GONE);
                        spnLevel.setVisibility(View.GONE);
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(oRecommend.getMessage());
                    }else{
                        txtPrescriptionTitle.setText(getString(R.string.kine_own));
                        txtPrescriptionDesc.setText(getString(R.string.wrist_ex));
                        txtKineOwnSpin.setVisibility(View.VISIBLE);
                        txtLevel.setVisibility(View.VISIBLE);
                        txtDuration.setVisibility(View.VISIBLE);
                        spnKine.setEnabled(true);
                        spnKine.setVisibility(View.VISIBLE);
                        spnLevel.setVisibility(View.VISIBLE);
                        spnDuration.setVisibility(View.VISIBLE);
                        spnLevel.setEnabled(true);
                        spnDuration.setEnabled(true);
                        txtMessage.setVisibility(View.GONE);
                    }


                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
            txtPrescriptionTitle.invalidate();
            txtPrescriptionDesc.invalidate();
            txtKineOwnSpin.invalidate();
            spnKine.invalidate();
            txtDuration.invalidate();
            txtLevel.invalidate();
            spnDuration.invalidate();
            spnLevel.invalidate();
        }
    }

}
