package com.yz_wss_sws901_termproject.ui.patient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Location;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.service.LocationReportingService;
import com.yz_wss_sws901_termproject.tools.GPSTracker;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeoutException;


public class LocationFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;



    private Patient oPatient = null;
    private GoogleMap mMap = null;

    private View rootView = null;
    private Context mContext = null;

    private Switch aSwitch = null;

    private ArrayList<Location> arrLocs;

    private SharedPreferences app_preferences;
    private Spinner spnPeriod = null;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LocationFragment.
     */
    public static LocationFragment newInstance(String param1, String param2) {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public LocationFragment() {
        // Required empty public constructor
    }

    private void populateViewForOrientation(LayoutInflater inflater, ViewGroup viewGroup) {
        viewGroup.removeAllViewsInLayout();

        if(mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            rootView = inflater.inflate(R.layout.fragment_location_horizontal, viewGroup);
        }else{
            rootView = inflater.inflate(R.layout.fragment_location, viewGroup);
        }
        mContext = rootView.getContext();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);


        if(mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            TextView txtPeriod =(TextView) rootView.findViewById(R.id.txtPeriod);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) txtPeriod.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.txtTitle);
            params.addRule(RelativeLayout.END_OF, R.id.switchLoc);
            params.addRule(RelativeLayout.RIGHT_OF, R.id.switchLoc);
            params.addRule(RelativeLayout.ALIGN_START);
            params.addRule(RelativeLayout.ALIGN_LEFT);

            //params.removeRule(RelativeLayout.ALIGN_START);
            //params.removeRule(RelativeLayout.ALIGN_LEFT);

            Spinner spnPeriod = (Spinner) rootView.findViewById(R.id.spnPeriod);
            params = (RelativeLayout.LayoutParams) spnPeriod.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.txtTitle);

            txtPeriod.invalidate();
            spnPeriod.invalidate();
        }else{
            TextView txtPeriod =(TextView) rootView.findViewById(R.id.txtPeriod);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) txtPeriod.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.switchLoc);
            params.addRule(RelativeLayout.ALIGN_START,R.id.switchLoc);
            params.addRule(RelativeLayout.ALIGN_LEFT,R.id.switchLoc);

            params.addRule(RelativeLayout.END_OF);
            params.addRule(RelativeLayout.RIGHT_OF);
            //params.removeRule(RelativeLayout.END_OF);
            //params.removeRule(RelativeLayout.RIGHT_OF);

            Spinner spnPeriod = (Spinner) rootView.findViewById(R.id.spnPeriod);
            params = (RelativeLayout.LayoutParams) spnPeriod.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.switchLoc);
            txtPeriod.invalidate();
            spnPeriod.invalidate();
        }


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public boolean isRunning(){
        /*
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if("com.yz_wss_sws901_termproject.service.LocationReportingService".equals(service.service.getClassName()))
            {
                return true;
            }
        }
        */
        if(app_preferences == null){
            String sharedPref = getString(R.string.sharedPref);
            app_preferences = getActivity().getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        }
        boolean isChecked = app_preferences.getBoolean("locationReporting", false);
        if(isChecked)
            return true;
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle extras = getActivity().getIntent().getExtras();
        if(oPatient == null)
            oPatient = extras.getParcelable("user");

        // Inflate the layout for this fragment
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
                rootView = null;
            }
        }
        try {

            if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                rootView = inflater.inflate(R.layout.fragment_location_horizontal, container, false);
            }else{
                rootView = inflater.inflate(R.layout.fragment_location, container, false);
            }
            if(mContext==null)
                mContext = rootView.getContext();
            initMap();

        } catch (InflateException e) {
            /* map is already there, just return view as it is */
            return rootView;
        } finally{
            aSwitch = (Switch) rootView.findViewById(R.id.switchLoc);
            spnPeriod = (Spinner) rootView.findViewById(R.id.spnPeriod);
            if(app_preferences == null){
                String sharedPref = getString(R.string.sharedPref);
                app_preferences = getActivity().getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
            }

            if(isRunning()){
                aSwitch.setChecked(true);
                spnPeriod.setEnabled(false);
            }else{
                spnPeriod.setEnabled(true);
            }

            final int nPeriod = app_preferences.getInt("reportingPeriod", 7);
            spnPeriod.setSelection(nPeriod);
            spnPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SharedPreferences.Editor editor = app_preferences.edit();
                    editor.remove("reportingPeriod");
                    editor.putInt("reportingPeriod", position);
                    Log.e("Spinner Set: ", "Period time is "+position);
                    editor.apply();
                    editor.commit();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            aSwitch.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if(app_preferences == null){
                                String sharedPref = getString(R.string.sharedPref);
                                app_preferences = getActivity().getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
                            }
                            SharedPreferences.Editor editor = app_preferences.edit();
                            if (isChecked) {
                                Intent myIntent = new Intent(mContext, LocationReportingService.class);
                                myIntent.putExtra("patientid", oPatient.getId());
                                getActivity().startService(myIntent);
                                editor.putBoolean("locationReporting", isChecked);
                                spnPeriod.setEnabled(false);
                                spnPeriod.invalidate();
                            } else {
                                getActivity().stopService(new Intent(mContext, LocationReportingService.class));
                                editor.putBoolean("locationReporting", isChecked);
                                spnPeriod.setEnabled(true);
                                spnPeriod.invalidate();
                            }
                            editor.apply();
                            editor.commit();
                        }
                    }
                );


            new GetData(mContext, oPatient.getId()).execute("");
            return rootView;
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();



        MapFragment f = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (f != null) {
            try {
                getFragmentManager().beginTransaction().remove(f).commit();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((PatientMainActivity) activity).onSectionAttached(2);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    private void initMap(){
        double latitude;
        double longitude;

        final Geocoder coder = new Geocoder(mContext);



        if(mMap == null){
            MapFragment mapFrag = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mMap = mapFrag.getMap();
            if(mMap == null){
                Toast.makeText(mContext, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
            else{
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                try {
                    //List<Address> geocodeResults = coder.getFromLocationName(oRest.getAddr(), 1);
                    //Iterator<Address> locations = geocodeResults.iterator();

                    //if (!locations.hasNext()) {
                    //    Toast.makeText(mContext, "Cannot find location", Toast.LENGTH_SHORT).show();
                    //}
                    GPSTracker gpsTracker = new GPSTracker(mContext);
                    android.location.Location loc = gpsTracker.getLocation();
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mMap.getUiSettings().setCompassEnabled(true);

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(
                            new LatLng(loc.getLatitude(), loc.getLongitude())).zoom(16).build();
                    /*MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(oRest.getRestName()).snippet("Phone Number: "+oRest.getPhone());
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    mMap.addMarker(marker);

                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener(){

                        @Override
                        public void onInfoWindowClick(Marker arg0) {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:"+Uri.encode( oRest.getPhone() )));
                            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(callIntent);
                        }

                    });*/

                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                }catch (Exception e) {
                    Log.e("Mapping", "Failed to get location info", e);
                    Toast.makeText(mContext, "Sorry! Error occured while get location", Toast.LENGTH_SHORT).show();
                }


            }

        }

    }

    private class GetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;
        private String patientId;

        private GetData(Context context, String patientId) {
            this.mContext = context;
            this.patientId = patientId;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.getLocationList(patientId);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {
                Toast.makeText(mContext, "no location history", Toast.LENGTH_LONG);
            } else {
                Log.e("onPostExecute", result);
                try {
                    arrLocs = ParseData.getLocationList(result);

                    if(arrLocs != null){
                        Location prevLoc = null;
                        for(Location aLoc : arrLocs){
                            Log.e("Locs", "Lat:"+aLoc.getLatitude()+", Log"+aLoc.getLongitude());
                            MarkerOptions marker =
                                    new MarkerOptions()
                                            .position(new LatLng(aLoc.getLatitude(), aLoc.getLongitude()))
                                            .title(oPatient.getFirstname() + ", " + oPatient.getLastname())
                                            .snippet(aLoc.getRecdate());
                            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            mMap.addMarker(marker);
                            if(prevLoc != null){
                                mMap.addPolyline(
                                        new PolylineOptions()
                                                .add(new LatLng(aLoc.getLatitude(), aLoc.getLongitude())
                                                        , new LatLng(prevLoc.getLatitude(), prevLoc.getLongitude()))
                                                .width(5)
                                                .color(getResources().getColor(R.color.wallet_secondary_text_holo_dark))
                                                .geodesic(true)) ;
                            }

                            prevLoc = aLoc;
                        }
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
