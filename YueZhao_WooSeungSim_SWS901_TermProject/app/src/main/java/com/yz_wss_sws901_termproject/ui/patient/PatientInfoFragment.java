package com.yz_wss_sws901_termproject.ui.patient;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.CommonResult;
import com.yz_wss_sws901_termproject.beans.History;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;


public class PatientInfoFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private Context mCtx;
    private Patient oPatient;

    EditText txtUser;
    EditText txtFirst;
    EditText txtLast;
    EditText txtTelephone;
    EditText txtEmail;
    EditText txtAddress;
    EditText txtEmerEmail;
    EditText txtEmerTelephone;
    Button btnUpdate;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PatientInfoFragment.
     */
    public static PatientInfoFragment newInstance(String param1, String param2) {
        PatientInfoFragment fragment = new PatientInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public PatientInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_patient_info, container, false);
        mCtx = rootView.getContext();

        Bundle extras = getActivity().getIntent().getExtras();
        oPatient = extras.getParcelable("user");



        Log.e("PatientInfo:", oPatient.toString() );

        txtUser = (EditText) rootView.findViewById(R.id.txtUsername);
        txtFirst = (EditText) rootView.findViewById(R.id.txtFirst);
        txtLast = (EditText) rootView.findViewById(R.id.txtLast);
        txtTelephone = (EditText) rootView.findViewById(R.id.txtTelephone);
        txtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        txtAddress = (EditText) rootView.findViewById(R.id.txtAddress);
        txtEmerEmail = (EditText) rootView.findViewById(R.id.txtEmerEmail);
        txtEmerTelephone = (EditText) rootView.findViewById(R.id.txtEmerTelephone);
        btnUpdate = (Button) rootView.findViewById(R.id.button3);

        fillTextBox();


        txtUser.setEnabled(false);
        txtFirst.setEnabled(false);
        txtLast.setEnabled(false);
        txtTelephone.setEnabled(false);
        txtEmail.setEnabled(false);
        txtAddress.setEnabled(false);
        txtEmerEmail.setEnabled(false);
        txtEmerTelephone.setEnabled(false);
        btnUpdate.setText(getString(R.string.edit));
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editInfo();
            }
        });


        return rootView;
    }

    public void updateInfo(){
        txtFirst.setEnabled(false);
        txtLast.setEnabled(false);
        txtTelephone.setEnabled(false);
        txtEmail.setEnabled(false);
        txtAddress.setEnabled(false);
        txtEmerEmail.setEnabled(false);
        txtEmerTelephone.setEnabled(false);
        btnUpdate.setText(getString(R.string.edit));
        btnUpdate.invalidate();
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editInfo();
            }
        });
        new SetData(mCtx).execute("");
    }

    public void editInfo(){
        txtFirst.setEnabled(true);
        txtLast.setEnabled(true);
        txtTelephone.setEnabled(true);
        txtEmail.setEnabled(true);
        txtAddress.setEnabled(true);
        txtEmerEmail.setEnabled(true);
        txtEmerTelephone.setEnabled(true);
        btnUpdate.setText(getString(R.string.update));
        btnUpdate.invalidate();
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateInfo();
            }
        });
    }

    private void fillTextBox(){
        if(oPatient != null){
            txtUser.setText(oPatient.getUsername());
            txtFirst.setText(oPatient.getFirstname());
            txtLast.setText(oPatient.getLastname());
            txtTelephone.setText(oPatient.getTelephone());
            txtEmail.setText(oPatient.getEmail());
            txtAddress.setText(oPatient.getAddress());
            txtEmerEmail.setText(oPatient.getEmer_email());
            txtEmerTelephone.setText(oPatient.getEmer_phone());
        }
    }

    private void invalidateViews(){
        txtUser.invalidate();
        txtFirst.invalidate();
        txtLast.invalidate();
        txtTelephone.invalidate();
        txtEmail.invalidate();
        txtAddress.invalidate();
        txtEmerEmail.invalidate();
        txtEmerTelephone.invalidate();
        btnUpdate.invalidate();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((PatientMainActivity) activity).onSectionAttached(1);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class SetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;



        private SetData(Context context) {
            this.mContext = context;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            Patient tempPatient = new Patient();
            tempPatient.setId(oPatient.getId());
            tempPatient.setLastname(txtLast.getText().toString());
            tempPatient.setFirstname(txtFirst.getText().toString());
            tempPatient.setTelephone(txtTelephone.getText().toString());
            tempPatient.setEmail(txtEmail.getText().toString());
            tempPatient.setAddress(txtAddress.getText().toString());
            tempPatient.setEmer_phone(txtEmerTelephone.getText().toString());
            tempPatient.setEmer_email(txtEmerEmail.getText().toString());

            WebRequest request = new WebRequest(mContext);

            try {
                result = request.updatePatientInfo(tempPatient);
                Log.e("result update:", result);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {
                Log.e("update Error:", result);
                Toast.makeText(mContext, "Error was occurred while updating ", Toast.LENGTH_LONG).show();
                fillTextBox();
                invalidateViews();
            } else {

                Log.e("onPostExecute", result);
                try {
                    Patient tempPatient = ParseData.parsePatient(result);

                    if(tempPatient!=null){
                        Toast.makeText(mContext, "Your information updated successfully", Toast.LENGTH_SHORT).show();
                        oPatient = tempPatient;
                        fillTextBox();
                        getActivity().getIntent().putExtra("user", (Serializable) oPatient);
                    }else{
                        Toast.makeText(mContext, "Error was occurred while updating ", Toast.LENGTH_LONG).show();
                        fillTextBox();
                    }
                } catch (JsonSyntaxException e) {
                    Toast.makeText(mContext, "Error was occurred while updating ", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } finally {
                    invalidateViews();
                }
            }

        }
    }

}
