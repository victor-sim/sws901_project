package com.yz_wss_sws901_termproject.ui.patient;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.R.id;
import com.yz_wss_sws901_termproject.R.layout;
import com.yz_wss_sws901_termproject.R.menu;
import com.yz_wss_sws901_termproject.beans.Recommend;
import com.yz_wss_sws901_termproject.tools.AccelerometerListener;
import com.yz_wss_sws901_termproject.tools.AccelerometerManager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends Activity implements AccelerometerListener{

	private Vibrator mVib;
	private GameAnimatedView gameView;
	private ImageView imgProgress;
	private Canvas canvas;

    private Recommend oRecommend;
    private boolean isWrist;
    private int level=1;
    private int duration = 2000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.activity_game);

        Bundle extras = getIntent().getExtras();
        oRecommend = getIntent().getParcelableExtra("recommend");

        if(oRecommend != null){
            isWrist = oRecommend.getIsWrist();
            level = oRecommend.getLevel();
            duration = oRecommend.getDuration();
        }else{
            isWrist = getIntent().getBooleanExtra("isWrist", false);
            level = getIntent().getIntExtra("level", 1);
            duration = getIntent().getIntExtra("duration", 12);
        }



        gameView = (GameAnimatedView) findViewById(R.id.gameAnimatedView1);
        if(isWrist) {
            gameView.setDrawableId(R.drawable.shake);
            setTitle(getString(R.string.wrist));
        }else {
            gameView.setDrawableId(R.drawable.swing);
            setTitle(getString(R.string.shoulder));
        }
		Log.e("GameActivity", "onCreate()");
		//Toast.makeText(getBaseContext(), "onCreate", Toast.LENGTH_SHORT).show();
		
		imgProgress = (ImageView) this.findViewById(R.id.imageView1);
		Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		Log.e("Progress View", "Width: " +size.x+", Height: "+size.y);
	    Bitmap bitmap = Bitmap.createBitmap(
	    		size.x,
                (int)(size.y*0.03),
	    		Bitmap.Config.ARGB_8888);
	    canvas = new Canvas(bitmap);
	    imgProgress.setImageBitmap(bitmap);
	    Paint paint = new Paint();
	    paint.setColor(Color.DKGRAY);
	    paint.setStyle(Paint.Style.FILL_AND_STROKE);
	    paint.setStrokeWidth(10);
	    float leftx = 0;
	    float topy = 0;
	    float rightx = (float)bitmap.getWidth();
	    float bottomy = (float)bitmap.getHeight();
	    canvas.drawRect(leftx, topy, rightx, bottomy, paint);
	}
	
	@Override
    public void onResume() {
		super.onResume();
		Log.e("GameActivity", "onResume()");
		//Toast.makeText(getBaseContext(), "onResume Start Accelerometer", Toast.LENGTH_SHORT).show();
		mVib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        final TextView txtDesc = (TextView) findViewById(R.id.txtDesc);

        //Check device supported Accelerometer sensor or not
        if (AccelerometerManager.isSupported(this)) {
            //Start Accelerometer Listening
            AccelerometerManager.startListening(this);
            //Toast.makeText(getBaseContext(), "onResume Accelerometer Started", Toast.LENGTH_SHORT).show();
        }

        float threshold = 15;
        int interval = 1000;
        float goal = 2000;

        goal = duration * 1000;

        if(isWrist){
            switch(level){
                case 1:
                    threshold = 10;
                    txtDesc.setText("Wrist Exercise, Easy");
                    break;
                case 2:
                    threshold = 17;
                    txtDesc.setText("Wrist Exercise, Normal");
                    break;
                case 3:
                default:
                    threshold = 23;
                    txtDesc.setText("Wrist Exercise, Hard");
                    break;
            }
        }else{
            interval = 100;
            switch(level){
                case 1:
                    threshold = 5;
                    txtDesc.setText("Shoulder Exercise, Easy");
                    break;
                case 2:
                    threshold = 7;
                    txtDesc.setText("Shoulder Exercise, Normal");
                    break;
                case 3:
                default:
                    threshold = 11;
                    txtDesc.setText("Shoulder Exercise, Hard");
                    break;
            }
        }

        AccelerometerManager.setGoal(goal);
        AccelerometerManager.configure(threshold, interval);


	}
	
	@Override
    public void onStop() {
		super.onStop();
		Log.e("GameActivity", "onStop()");
		//Toast.makeText(getBaseContext(), "onStop Accelerometer Stoped", Toast.LENGTH_SHORT).show();
		//Check device supported Accelerometer sensor or not
		if (AccelerometerManager.isListening()) {
			//Start Accelerometer Listening
			AccelerometerManager.stopListening();
		}
	}
	
	@Override
    public void onDestroy() {
		super.onDestroy();
		//Toast.makeText(getBaseContext(), "onDestroy Accelerometer Stoped", Toast.LENGTH_SHORT).show();
		Log.e("GameActivity", "onDestroy()");
		//Check device supported Accelerometer sensor or not
		if (AccelerometerManager.isListening()) {
			//Start Accelerometer Listening
			AccelerometerManager.stopListening(); 
		}
	         
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

    /*
	public void applyValue(View view){
		//if (AccelerometerManager.isListening()) {
			final EditText txtThre = (EditText) findViewById(R.id.txtThre);
	        final EditText txtInt = (EditText) findViewById(R.id.txtInt);
	        float threshold = Float.parseFloat(txtThre.getText().toString());
	        int interval = Integer.parseInt(txtInt.getText().toString());
			AccelerometerManager.configure(threshold, interval);
		//}
	}*/
	
	@Override
	public void setProgress(float progress){
		//Bitmap bitmap = Bitmap.createBitmap(imgProgress.getWidth(), imgProgress.getHeight(), Bitmap.Config.ARGB_8888);
	    //canvas = new Canvas(bitmap);
	    //imgProgress.setImageBitmap(bitmap);
		
		if(progress > 1){
			progress = 1;
		}
	    Paint paint = new Paint();
	    paint.setColor(Color.BLUE);
	    paint.setStyle(Paint.Style.FILL_AND_STROKE);
	    paint.setStrokeWidth(2);
	    
	    Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		//Log.e("Progress View", "Width: " +canvas.getWidth()+", Height: "+canvas.getHeight());
	    
	    float leftx = (float)canvas.getWidth() * 0.05f;
	    float topy = (float)canvas.getHeight() * 0.2f;
	    float rightx = leftx + (float)canvas.getWidth() * 0.9f * progress;
	    float bottomy = (float)canvas.getHeight() *0.8f;
	    canvas.drawRect(leftx, topy, rightx, bottomy, paint);
	    imgProgress.invalidate();
	}
	
	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShake(float force) {
		if (AccelerometerManager.isListening()) {
			//Start Accelerometer Listening
			AccelerometerManager.stopListening(); 
		}
		//Toast.makeText(getBaseContext(), "Motion detected", Toast.LENGTH_SHORT).show();
		mVib.vibrate(2000);
		gameView.setDrawableId(R.drawable.congratulation);
		
	}
}
