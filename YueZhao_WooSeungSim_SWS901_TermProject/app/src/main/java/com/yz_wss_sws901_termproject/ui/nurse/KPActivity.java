package com.yz_wss_sws901_termproject.ui.nurse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.CommonResult;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.beans.Recommend;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;
import com.yz_wss_sws901_termproject.ui.patient.PatientMainActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class KPActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kp);

        final Patient patient=getIntent().getParcelableExtra("patient");
        TextView textView= (TextView) findViewById(R.id.tv_KP_PatientName);
        textView.setText("Patient Name: "+patient.getFirstname()+" "+patient.getLastname());

        Button btn_SendKP = (Button) findViewById(R.id.btn_SendKP);
        btn_SendKP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendKP(patient);
            }
        });

        Button btn_CancelKP= (Button) findViewById(R.id.btn_CancelSendKP);
        btn_CancelKP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        DatePicker datePicker_KP= (DatePicker) findViewById(R.id.date_picker_KPDueDate);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR,0);

        datePicker_KP.setMinDate(cal.getTimeInMillis());

        new GetData(this, patient.getId()).execute("");

    }

    protected void sendKP(Patient patient) {
        RadioGroup radioGroup_GameType = (RadioGroup) findViewById(R.id.radioGroup_GameType);
        RadioGroup radioGroup_GameSpeed = (RadioGroup) findViewById(R.id.radioGroup_GameSpeed);
        RadioGroup radioGroup_GameDuration = (RadioGroup) findViewById(R.id.radioGroup_GameDuration);
        EditText et_exhort= (EditText) findViewById(R.id.editText_exhort);
        DatePicker datePicker_KP= (DatePicker) findViewById(R.id.date_picker_KPDueDate);

        String exhortation=et_exhort.getText().toString();

        Date due=new Date(datePicker_KP.getCalendarView().getDate());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDueDate = dateFormat.format(due);

        Boolean isWrist=false;
        switch (radioGroup_GameType.getCheckedRadioButtonId()){
            case R.id.radioGroup_GameType_Wrist:
                isWrist=true;
                break;
            case R.id.radioGroup_GameType_Shoulder:
                isWrist=false;
        }


        int level=0;
        switch (radioGroup_GameSpeed.getCheckedRadioButtonId()){
            case R.id.radioGroup_GameSpeed_slow:
                level=1;
                break;
            case R.id.radioGroup_GameSpeed_normal:
                level=2;
                break;
            case R.id.radioGroup_GameSpeed_fast:
                level=3;
                break;
        }

        int duration=0;
        switch (radioGroup_GameDuration.getCheckedRadioButtonId()){
            case R.id.radioGroup_GameDuration_short:
                duration=1;
                break;
            case R.id.radioGroup_GameDuration_normal:
                duration=2;
                break;
            case R.id.radioGroup_GameDuration_long:
                duration=3;
                break;
        }

        Recommend recommend=new Recommend();
        recommend.setPatientId(patient.getId());
        recommend.setIsWrist(isWrist);
        recommend.setLevel(level);
        recommend.setDuration(duration);
        recommend.setMessage(exhortation);
        recommend.setDueDate(formattedDueDate);


        new SendData(this, recommend).execute("");

    }

    private class GetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;
        private String patientId;



        private GetData(Context context, String patientId) {
            this.mContext = context;
            this.patientId = patientId;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.getRecommend(Integer.parseInt(patientId));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {

            }else{
                AlertDialog alertDialog1=new AlertDialog.Builder(mContext).create();

                //Setting Dialog Title
                alertDialog1.setTitle("Kinesiotheraphy Prescription");

                //Setting Dialog Message
                alertDialog1.setMessage("This patient already have kinesiotheraphy prescription");

                //Setting Icon to Dialog
                alertDialog1.setIcon(R.drawable.ic_launcher);

                //Setting OK Button
                alertDialog1.setButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        finish();
                    }
                });

                //Showing Alert Message
                alertDialog1.show();
            }
        }
    }

    private class SendData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;
        private Recommend recommend;
        //private int mType;

        private SendData(Context context, Recommend recommend) {
            this.mContext = context;
            this.recommend = recommend;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.addRecommend(recommend);
                Log.e("KPActivity", "add new recommend request result is " + result);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("") || result.equals("null")) {
                Toast.makeText(mContext, "Error while send data", Toast.LENGTH_LONG);
            } else {
                Log.e("MainActivity", "result is "+ result);
                CommonResult comResult = ParseData.CommonPares(result);
                if (Integer.parseInt(comResult.getResult() ) >0 ){
                    Toast.makeText(mContext, "Sent recommend successfully!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext, "Error while send data", Toast.LENGTH_LONG);
                }


                AlertDialog alertDialog1=new AlertDialog.Builder(mContext).create();

                //Setting Dialog Title
                alertDialog1.setTitle("Send Kinesiotheraphy");

                //Setting Dialog Message
                alertDialog1.setMessage("New kinesiotheraphy prescription was sent");

                //Setting Icon to Dialog
                alertDialog1.setIcon(R.drawable.ic_launcher);

                //Setting OK Button
                alertDialog1.setButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        finish();
                    }
                });

                //Showing Alert Message
                alertDialog1.show();


            }
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_k, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loginIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
