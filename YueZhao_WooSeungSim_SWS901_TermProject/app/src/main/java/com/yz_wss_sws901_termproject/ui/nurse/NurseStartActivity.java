package com.yz_wss_sws901_termproject.ui.nurse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.beans.User;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;

public class NurseStartActivity extends Activity {

    private ArrayList<Patient> arrPatient;
    private PatientBase mAdapter;
    private ListView lstPatient;
    private User oUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nurse_start);
        new GetData(this).execute("");
        lstPatient = (ListView) findViewById(R.id.lstPatient);

        Bundle extras = getIntent().getExtras();
        oUser = getIntent().getParcelableExtra("user");

        mAdapter = new PatientBase();
        lstPatient.setAdapter(mAdapter);
        registerForContextMenu(lstPatient);
        lstPatient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Patient patient = arrPatient.get(position);
                Intent intent = new Intent(getBaseContext(), VitalSignsActivity.class);
                intent.putExtra("patient", (android.os.Parcelable) patient);
                //startActivityForResult(intent,0);
                startActivity(intent);
            }
        });
    }

    //Context Menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.context_menu_patient_list, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Patient patient = arrPatient.get(info.position);

        switch (item.getItemId()) {
            case R.id.context_menu_edit:
                Intent intent1 = new Intent(getBaseContext(), PatientDetailActivity.class);
                intent1.putExtra("patient", (android.os.Parcelable) patient);
                startActivity(intent1);
                break;
            case R.id.context_menu_send:
                Uri smsTo = Uri.parse("smsto:" + patient.getTelephone());
                Intent intent2 = new Intent(Intent.ACTION_SENDTO, smsTo);
                startActivity(intent2);
                break;
            case R.id.context_menu_history:
                Intent intent3 = new Intent(getBaseContext(), PatientHistoryActivity.class);
                intent3.putExtra("patient", (android.os.Parcelable) patient);
                startActivity(intent3);
                break;
            case R.id.context_menu_kp:
                Intent intent4=new Intent(getBaseContext(),KPActivity.class);
                intent4.putExtra("patient", (android.os.Parcelable) patient);
                startActivity(intent4);
                break;
            case R.id.context_menu_map:
                Intent intent5=new Intent(getBaseContext(),PatientMapsActivity.class);
                intent5.putExtra("patient", (android.os.Parcelable) patient);
                startActivity(intent5);
                break;
        }
        return super.onContextItemSelected(item);
    } //Context Menu End

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nurse_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loginIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class PatientBase extends BaseAdapter {
        @Override
        public int getCount() {
            if (arrPatient == null)
                return 0;
            return arrPatient.size();
        }

        @Override
        public Object getItem(int position) {
            return arrPatient.get(position);
        }

        @Override
        public long getItemId(int position) {
            long nId;
            nId = Long.parseLong(((Patient) getItem(position)).getId());
            return nId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (arrPatient == null) {
                return convertView;
            }
            final View view = convertView.inflate(NurseStartActivity.this, R.layout.patientlist_slot, null);

            Patient oPatient = (Patient) getItem(position);

            final TextView txtName = (TextView) view.findViewById(R.id.txtName);
            final TextView txtDate = (TextView) view.findViewById(R.id.txtDate);
            final TextView txtTelephone = (TextView) view.findViewById(R.id.txtTelephone);
            final TextView txtEmail = (TextView) view.findViewById(R.id.txtEmail);

            txtName.setText(oPatient.getFirstname() + ", " + oPatient.getLastname());

            txtDate.setText(oPatient.getRecDatebyFormatString());
            txtTelephone.setText("Phone: " + oPatient.getTelephone());
            txtEmail.setText(oPatient.getEmail());

            return view;
        }
    }

	
	
	private class GetData extends AsyncTask<String, String, String> {
		private Context mContext;
		private int mType;
        private DialogActivity myDialog;
		

		private GetData(Context context) {
			this.mContext = context;
            myDialog = new DialogActivity(context);
			//this.mType = type;
		}

		@Override
		protected void onPreExecute() {
			if (mType == 0) {
				;
			}
            myDialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			String result = null;
			WebRequest request = new WebRequest(
					NurseStartActivity.this);

			try {
				result = request.getPatientList();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
            myDialog.dismiss();
			if (result == null || result.equals("")) {
				Toast.makeText(mContext, "no result", Toast.LENGTH_LONG).show();
			} else {
				
				arrPatient =null;
				try {
					arrPatient = new ParseData().getPatientList(result);
				} catch (JsonSyntaxException e) {
					e.printStackTrace();
				}
				if (arrPatient != null) {
					/*Intent intent = new Intent(PatientListActivity.this,
							PatientListActivity.class);
					intent.putExtra("patients", (Serializable) arrPatient);
					startActivity(intent);
					PatientListActivity.this.finish();*/
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(mContext, "Patient List updated", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Some error on patient list", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
