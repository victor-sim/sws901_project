package com.yz_wss_sws901_termproject.ui.common;


import com.yz_wss_sws901_termproject.R;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class LogInFragment extends Fragment{
    private String szFrag = "LogInFragment";
    
    private EditText txtUsername;
    private EditText txtPassword;
    private CheckBox chkKeepLogin;
    
    public String getUsername(){
    	if(txtUsername == null){
    		Log.e("getUserName()", "View is null");
    		return null;
    	}else if (txtUsername.getText() == null){
    		Log.e("getUserName()", "Text is null");
    		return null;
    	}
    	return txtUsername.getText().toString();
    }
    
    public String getPassword(){
    	if(txtPassword == null){
    		Log.e("getPassword()", "View is null");
    		return null;
    	}else if (txtPassword.getText() == null){
    		Log.e("getPassword()", "Text is null");
    		return null;
    	}
    	return txtPassword.getText().toString();
    }

    public boolean getChecked(){
        if(chkKeepLogin == null){
            return false;
        }else{
            return chkKeepLogin.isChecked();
        }
    }


	public LogInFragment(){
        Log.d(szFrag, "Default Constructor");
	}
	
	@Override
    public void onResume() {
		super.onResume();
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, 
    						ViewGroup container, Bundle savedInstanceState) {
		
		getActivity().getActionBar().show();
		View rootView = inflater.inflate(R.layout.fragment_login, container, false);
		
		//WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
		//Display display = wm.getDefaultDisplay();

		//int orientation = display.getOrientation();
		
		//final Button btnSignUp = (Button) getActivity().findViewById(R.id.btnSignUp);
		//final Button btnLogIn = (Button) getActivity().findViewById(R.id.btnLogIn);
		//final EditText editTxtId = (EditText) getActivity().findViewById(R.id.editTxtId);
		//final EditText editTxtPassword = (EditText) getActivity().findViewById(R.id.editTxtPassword);

        //final Spinner spnType = (Spinner) rootView.findViewById(R.id.spnType);
        txtUsername = (EditText) rootView.findViewById(R.id.editUsername);
        txtPassword = (EditText) rootView.findViewById(R.id.editPass);
        chkKeepLogin = (CheckBox) rootView.findViewById(R.id.keepLogin);
        final Button btnSignUp = (Button) rootView.findViewById(R.id.btnSignUp);

        String szUserName = getArguments().getString("username");
        String szPassword = getArguments().getString("password");
        boolean bChecked = getArguments().getBoolean("keep");

        if(szUserName != null){
            if(txtUsername!=null)
                txtUsername.setText(szUserName);
        }
        if(szPassword != null){
            if(txtPassword!=null)
                txtPassword.setText(szPassword);
        }

        chkKeepLogin.setChecked(bChecked);
        Log.e("Log In Fragment", "onCreateView()");



		//Toast.makeText( rootView.getContext(), "Test ID: test01\nPassword: testtest", Toast.LENGTH_LONG).show();

		return rootView;
		
	}
	
	public void logIn(View view){

        ((OnButtonSelect)getActivity()).logIn(txtUsername.getText().toString(),
                                                txtPassword.getText().toString(),
                                                chkKeepLogin.isChecked());
	}
	
	public void signUp(View view){
		((OnButtonSelect)getActivity()).signUp(view);
	}
	
	
	public interface OnButtonSelect {
        public void signUp(View view);
        public void logIn(String username, String password, boolean isChecked);
        public void setSpinner(Spinner spnUser);
    }

}
