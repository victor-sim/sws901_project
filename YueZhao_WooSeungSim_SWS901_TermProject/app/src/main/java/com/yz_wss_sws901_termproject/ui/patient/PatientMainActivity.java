package com.yz_wss_sws901_termproject.ui.patient;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.support.v4.widget.DrawerLayout;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.ui.common.LogInFragment;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;

public class PatientMainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    Patient oPatient;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_main);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fm = getFragmentManager();
        Fragment fr;

        switch(position+1){
            case 1:
                fr = new PatientInfoFragment();
                break;
            case 2:
                fr = new LocationFragment();
                break;
            case 3:
                fr = new GameFragment();
                break;
            case 4:
                fr = new PatientInfoFragment();
                break;
            case 5:
            default:
                fr = new PatientInfoFragment();
                break;
        }

        fm.beginTransaction()
                .replace(R.id.container, fr)
                .commit();
        onSectionAttached(position+1);
        ActionBar actionBar = getActionBar();
        actionBar.setTitle(mTitle);

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.patient_main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            finish();
            startActivity(loginIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void doOwn(View view){
        GameFragment aFrag = (GameFragment)getFragmentManager().findFragmentById(R.id.container);
        aFrag.runGame();

        return;
    }

    public void doPrescription(View view){
        Intent intent = new Intent(this , GameActivity.class);
        startActivity(intent);
        return;
    }

}
