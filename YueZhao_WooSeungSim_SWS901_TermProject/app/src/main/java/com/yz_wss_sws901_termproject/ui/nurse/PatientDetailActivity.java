package com.yz_wss_sws901_termproject.ui.nurse;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;

public class PatientDetailActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_detail);

        TextView tv_fn= (TextView) findViewById(R.id.tv_FirstName);
        TextView tv_ln= (TextView) findViewById(R.id.tv_LastName);
        TextView tv_tele= (TextView) findViewById(R.id.tv_Telephone);
        TextView tv_email= (TextView) findViewById(R.id.tv_Email);
        TextView tv_address= (TextView) findViewById(R.id.tv_Address);

        final Patient patient = getIntent().getParcelableExtra("patient");
        tv_fn.setText(patient.getFirstname());
        tv_ln.setText(patient.getLastname());
        tv_tele.setText(patient.getTelephone());
        tv_email.setText(patient.getEmail());
        tv_address.setText(patient.getAddress());

        tv_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail(patient);
            }
        });

        tv_tele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeCall(patient);
            }
        });

        Button btn_Return = (Button) findViewById(R.id.btn_Return);
        btn_Return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void sendEmail(Patient p) {
        Log.i("Send email", "");

        String[] TO = {p.getEmail()};
       // String[] CC = {"mcmohd@gmail.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");


        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
//        emailIntent.putExtra(Intent.EXTRA_CC, CC);
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
//        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getBaseContext(),
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    protected void makeCall(Patient p) {
        Log.i("Make call", "");

        Intent phoneIntent = new Intent(Intent.ACTION_CALL);
        phoneIntent.setData(Uri.parse("tel:"+p.getTelephone()));

        try {
            startActivity(phoneIntent);
            finish();
            Log.i("Finished making a call...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getBaseContext(),
                    "Call failed, please try again later.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_patient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loginIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
