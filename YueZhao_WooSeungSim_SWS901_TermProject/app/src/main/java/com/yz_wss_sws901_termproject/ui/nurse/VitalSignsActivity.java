package com.yz_wss_sws901_termproject.ui.nurse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.CommonResult;
import com.yz_wss_sws901_termproject.beans.History;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class VitalSignsActivity extends Activity implements NumberPickerFragment.OnNumberDialogDoneListener {
    private TextView tv_BodyTemp = null;
    private TextView tv_HeartRate = null;
    private TextView tv_HighBP = null;
    private TextView tv_LowBP = null;
    private TextView tv_Resp = null;

    private static final String defaultVS = "Click to input";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_signs);

        final Patient patient = getIntent().getParcelableExtra("patient");

        TextView tv_PatientName = (TextView) findViewById(R.id.tv_VitalSigns_PatientName);
        tv_PatientName.setText(patient.getFirstname() + " " + patient.getLastname());

        tv_BodyTemp = (TextView) findViewById(R.id.editText_BodyTemp);
        tv_BodyTemp.setText(defaultVS);
        tv_BodyTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerFragment picker = NumberPickerFragment.newInstance(3, 365, R.id.editText_BodyTemp);
                picker.show(getFragmentManager(), "test");
            }
        });

        tv_HeartRate = (TextView) findViewById(R.id.editText_HeartRate);
        tv_HeartRate.setText(defaultVS);
        tv_HeartRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerFragment picker = NumberPickerFragment.newInstance(2, 72, R.id.editText_HeartRate);
                picker.show(getFragmentManager(), "test");

            }
        });
        tv_HighBP = (TextView) findViewById(R.id.editText_HighBloodPressure);
        tv_HighBP.setText(defaultVS);
        tv_HighBP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerFragment picker = NumberPickerFragment.newInstance(3, 140, R.id.editText_HighBloodPressure);
                picker.show(getFragmentManager(), "test");
            }
        });
        tv_LowBP = (TextView) findViewById(R.id.editText_LowBloodPressure);
        tv_LowBP.setText(defaultVS);
        tv_LowBP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerFragment picker = NumberPickerFragment.newInstance(3, 80, R.id.editText_LowBloodPressure);
                picker.show(getFragmentManager(), "test");
            }
        });
        tv_Resp = (TextView) findViewById(R.id.editText_ResRate);
        tv_Resp.setText(defaultVS);
        tv_Resp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerFragment picker = NumberPickerFragment.newInstance(2, 12, R.id.editText_ResRate);
                picker.show(getFragmentManager(), "test");
            }
        });


        Button btn_Add = (Button) findViewById(R.id.btn_AddVitalSigns);
        Button btn_Cancel = (Button) findViewById(R.id.btn_CancelAddVitalSigns);

        btn_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addVitalSigns(patient);
                finish();
            }
        });

        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void addVitalSigns(Patient patient) {


        String bodyTemp = String.valueOf(tv_BodyTemp.getText());
        String heartRate = String.valueOf(tv_HeartRate.getText());
        String highBloodPressure = String.valueOf(tv_HighBP.getText());
        String lowBloodPressure = String.valueOf(tv_LowBP.getText());
        String resRate = String.valueOf(tv_Resp.getText());

        History history = new History(patient.getId(), bodyTemp, heartRate, highBloodPressure, lowBloodPressure, resRate, new Date());

        new SendData(this, history).execute("");

    }


    @Override
    public void onDone(int value, int viewID) {
        String returnVS = Integer.toString(value);
        TextView textView = (TextView) findViewById(viewID);
        if (viewID == R.id.editText_BodyTemp) {
            float temp = Float.valueOf(returnVS);
            returnVS = String.valueOf(temp / 10);
        }
        textView.setText(returnVS);
    }

    private class SendData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;
        private History history;
        //private int mType;

        private SendData(Context context, History history) {
            this.mContext = context;
            this.history = history;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.addHistory(history);
                Log.e("VitalSignsActivity", "add new history request result is " + result);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("") || result.equals("null")) {
                Toast.makeText(mContext, "Error while send data", Toast.LENGTH_LONG);
            } else {
                Log.e("MainActivity", "result is " + result);
                CommonResult comResult = ParseData.CommonPares(result);
                if (Integer.parseInt(comResult.getResult()) > 0) {
                    Toast.makeText(mContext, "Sent history successfully!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Error while send data", Toast.LENGTH_LONG);
                }


/*                AlertDialog alertDialog1=new AlertDialog.Builder(mContext).create();

                //Setting Dialog Title
                alertDialog1.setTitle("Send History");

                //Setting Dialog Message
                alertDialog1.setMessage("New history was sent");

                //Setting Icon to Dialog
                alertDialog1.setIcon(R.drawable.ic_launcher);

                //Setting OK Button
                alertDialog1.setButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        finish();
                    }
                });

                //Showing Alert Message
                alertDialog1.show();*/


            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vital_signs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loginIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
