package com.yz_wss_sws901_termproject.ui.nurse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;

public class PatientInfoActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_patient);

        EditText et_fn = (EditText) findViewById(R.id.editText_FirstName);
        EditText et_ln = (EditText) findViewById(R.id.editText_LastName);
        EditText et_tele = (EditText) findViewById(R.id.editText_Telephone);
        EditText et_email = (EditText) findViewById(R.id.editText_Email);
        EditText et_address = (EditText) findViewById(R.id.editText_Address);

        Patient patient = getIntent().getParcelableExtra("patient");
        et_fn.setText(patient.getFirstname());
        et_ln.setText(patient.getLastname());
        et_tele.setText(patient.getTelephone());
        et_email.setText(patient.getEmail());
        et_address.setText(patient.getAddress());

        Button btn_Confirm = (Button) findViewById(R.id.btn_ConfirmEdit);
        Button btn_Cancel = (Button) findViewById(R.id.btn_CancelEdit);

        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Edit successfully!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_patient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loginIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
