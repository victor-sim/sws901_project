package com.yz_wss_sws901_termproject.ui.common;


import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.R.id;
import com.yz_wss_sws901_termproject.R.layout;
import com.yz_wss_sws901_termproject.R.menu;
import com.yz_wss_sws901_termproject.beans.*;
import com.yz_wss_sws901_termproject.ui.nurse.NurseStartActivity;
import com.yz_wss_sws901_termproject.ui.patient.PatientMainActivity;
import com.yz_wss_sws901_termproject.web.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends Activity {

	private User oUser;
	private Patient oPatient;
	private boolean isPatient = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		Spinner spType = (Spinner) findViewById(R.id.spnType);
		
		final EditText emer_Phone = (EditText) findViewById(R.id.emerPhone);
		final EditText emer_Email = (EditText) findViewById(R.id.emerEmail);
		final TextView txtPhone = (TextView) findViewById(R.id.textView8);
		final TextView txtEmail = (TextView) findViewById(R.id.textView9);
		
		spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if(pos==0){
                	isPatient = true;
                	emer_Phone.setVisibility(View.VISIBLE);
                	emer_Email.setVisibility(View.VISIBLE);
                	txtPhone.setVisibility(View.VISIBLE);
                	txtEmail.setVisibility(View.VISIBLE);
                }else if(pos == 1){
                	isPatient = false;
                	emer_Phone.setVisibility(View.INVISIBLE);
                	emer_Email.setVisibility(View.INVISIBLE);
                	txtPhone.setVisibility(View.INVISIBLE);
                	txtEmail.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing, just another required interface callback
            }
        });
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_add) {
			EditText emer_Phone = (EditText) findViewById(R.id.emerPhone);
			EditText emer_Email = (EditText) findViewById(R.id.emerEmail);
			EditText userName = (EditText) findViewById(R.id.userName);
			EditText password = (EditText) findViewById(R.id.password);
			EditText firstname = (EditText) findViewById(R.id.firstName);
			EditText lastname = (EditText) findViewById(R.id.lastname);
			EditText telephone = (EditText) findViewById(R.id.telephone);
			EditText email = (EditText) findViewById(R.id.email);
			EditText address = (EditText) findViewById(R.id.address);
			
			if(emer_Phone.getText().length() > 0 || emer_Email.getText().length()>0){
				oPatient = new Patient();
				oPatient.setUsername(userName.getText().toString());
				oPatient.setPassword(password.getText().toString());
				oPatient.setFirstname(firstname.getText().toString());
				oPatient.setLastname(lastname.getText().toString());
				oPatient.setTelephone(telephone.getText().toString());
				oPatient.setEmail(email.getText().toString());
				oPatient.setAddress(address.getText().toString());
				oPatient.setEmer_phone(emer_Phone.getText().toString());
				oPatient.setEmer_email(emer_Email.getText().toString());
			}else{
				oUser = new User();
				oUser.setUsername(userName.getText().toString());
				oUser.setPassword(password.getText().toString());
				oUser.setFirstname(firstname.getText().toString());
				oUser.setLastname(lastname.getText().toString());
				oUser.setTelephone(telephone.getText().toString());
				oUser.setEmail(email.getText().toString());
				oUser.setAddress(address.getText().toString());
			}
			new SendData(this).execute("");
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class SendData extends AsyncTask<String, String, String>{
		private Context mContext;
        private DialogActivity myDialog;
		//private int mType;

		private SendData(Context context) {
			this.mContext = context;
            myDialog = new DialogActivity(context);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
            myDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			String result = null;
			WebRequest request = new WebRequest(mContext);
			
			try {
				if(oPatient != null)
					result = request.addPatient(oPatient);
				else
					result = request.addNurse(oUser);
				Log.e("Main Activity", "add new account request result is "+ result);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
			
			return result;
		}
		
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {
            myDialog.dismiss();
			if (result == null || result.equals("") || result.equals("null")) {
				Toast.makeText(mContext, "Error while send data", Toast.LENGTH_LONG);
			} else {
				Log.e("MainActivity", "result is "+ result);
				if(isPatient){
					oPatient = new ParseData().parsePatient(result);
					oUser = null;
				}else{
					oUser = new ParseData().parseUser(result);
					oPatient = null;
				}
				
				AlertDialog alertDialog1=new AlertDialog.Builder(mContext).create();
				
				//Setting Dialog Title
				alertDialog1.setTitle("Add new user");
				
				//Setting Dialog Message
				alertDialog1.setMessage("New user was created");
				
				//Setting Icon to Dialog
				alertDialog1.setIcon(R.drawable.ic_launcher);
				
				//Setting OK Button
				alertDialog1.setButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						if(isPatient){
							// invoke patient start screen
							Intent intent = new Intent(mContext , PatientMainActivity.class);
							intent.putExtra("user", (Serializable) oPatient);
							startActivity(intent);
							finish();
						}else{
							// invoke nurse start screen
							final Intent intent = new Intent(mContext , NurseStartActivity.class);
							intent.putExtra("user", (Serializable) oUser);
							startActivity(intent);
							finish();
						}

					}
				}); 
					
				//Showing Alert Message
				alertDialog1.show();
				
	
			}
		}
	}
}
