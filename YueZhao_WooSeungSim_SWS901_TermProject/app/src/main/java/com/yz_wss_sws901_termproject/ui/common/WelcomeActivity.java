package com.yz_wss_sws901_termproject.ui.common;


import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.*;
import com.yz_wss_sws901_termproject.service.LocationReportingService;
import com.yz_wss_sws901_termproject.ui.nurse.NurseStartActivity;
import com.yz_wss_sws901_termproject.ui.patient.PatientMainActivity;
import com.yz_wss_sws901_termproject.web.*;
import com.google.gson.JsonSyntaxException;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class WelcomeActivity extends Activity implements IntroFragment.OnTouchListener{
    private Fragment loginFragment;
    private User oUser;
    private Patient oPatient;
    private SharedPreferences app_preferences;
    private Context mContext;
    private boolean bAutoLogin = false;

    @Override
    protected void onStop(){
        super.onStop();
        //oUser = null;
        //oPatient = null;
    }

    @Override
    public void startLogInFragment(){
        if(loginFragment != null)
            return;
        loginFragment = new LogInFragment();


        String szId = app_preferences.getString("id", "");
        String szPassword = app_preferences.getString("password", "");
        boolean bCheck = app_preferences.getBoolean("keep",false);

        if(bCheck && szId.length()>0 && szPassword.length()>0){
            login(szId, szPassword, bCheck);
            bAutoLogin = true;
            return;
        }else{
            bAutoLogin = false;
        }

        Bundle bundle=new Bundle();
        bundle.putString("username", szId);
        bundle.putString("password", szPassword);
        bundle.putBoolean("keep", bCheck);
        loginFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container, loginFragment).commit();
    }

    public void startIntroFragment(){
        Fragment fr = new IntroFragment();
        getFragmentManager().beginTransaction().replace(R.id.container, fr).commit();
    }

    public void signUp(View view){
        Intent intent = new Intent(this , SignUpActivity.class);
        startActivity(intent);
        //finish();

    }

    public void login(View view){
        LogInFragment aFrag = (LogInFragment)getFragmentManager().findFragmentById(R.id.container);
        String username = aFrag.getUsername();
        String password = aFrag.getPassword();
        boolean isChecked = aFrag.getChecked();

        login(username, password, isChecked);
    }

    public void login(String username, String password, boolean isChecked){
        //LogInFragment aFrag = (LogInFragment)getFragmentManager().findFragmentById(R.id.container);
        Log.e("login start", "login()");
        if(app_preferences != null){
            String szId = app_preferences.getString("id", "");
            String szPassword = app_preferences.getString("password", "");
            boolean bChecked = app_preferences.getBoolean("keep", false);
            Log.e("login Pref", "id: "+szId + ", password: " + szPassword+", keep: "+bChecked);
            Log.e("login Parameter", "id: "+username + ", password: " + password+", keep: "+isChecked);

            SharedPreferences.Editor editor = app_preferences.edit();

            if(!isChecked){
                editor.remove("id");
                editor.remove("password");
                editor.remove("keep");
                editor.remove("report");
            }else{
                editor.putString("id",username);
                editor.putString("password",password);
                editor.putBoolean("keep", isChecked);
            }

            if(username == null || password == null){
                Toast.makeText(mContext, "Please fill all the form to log-in", Toast.LENGTH_LONG).show();
                return;
            }
            else if(username.length()==0 || password.length()==0)
            {
                Toast.makeText(mContext, "Please fill all the form to log-in", Toast.LENGTH_LONG).show();
                return;
            }

            editor.apply();
            editor.commit();

        }
        new GetData(mContext, username, password, isChecked).execute("");

    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.e("onResume()", "pref setted");

        String sharedPref = getString(R.string.sharedPref);
        app_preferences = getSharedPreferences(sharedPref, MODE_PRIVATE);
                //PreferenceManager.getDefaultSharedPreferences(this);

        if(loginFragment != null)
            startLogInFragment();
        else{
            Bundle extras = getIntent().getExtras();

            if(extras != null){
                String action = extras.getString("action");
                if(action != null && action.equals("login")){
                    startLogInFragment();
                    return;
                }else if (action!=null && action.equals("logout")){
                    if(app_preferences != null) {
                        SharedPreferences.Editor editor = app_preferences.edit();
                        editor.remove("password");
                        editor.remove("reportingPeriod");
                        editor.remove("locationReporting");
                        editor.remove("lastReport");
                        editor.apply();
                        editor.commit();
                    }
                    stopService(new Intent(mContext, LocationReportingService.class));
                    startLogInFragment();
                    return;
                }
            }
            startIntroFragment();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mContext = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            login(findViewById(R.id.btnLogin));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class GetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private String userName;
        private String password;
        private boolean isChecked;
        private DialogActivity myDialog;



        private GetData(Context context, String userName, String password, boolean isChecked) {
            this.mContext = context;
            this.userName = userName;
            this.password = password;
            this.isChecked = isChecked;

            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.login(userName, password);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {
                Toast.makeText(mContext, "no result", Toast.LENGTH_LONG);
            } else {
                boolean isLoggedIn = false;

                Log.e("onPostExecute", result);
                try {
                    isLoggedIn = ParseData.isLoggedIn(result);

                    if(isLoggedIn){
                        boolean isPatient = ParseData.isPatient(result);
                        if(isPatient){
                            oPatient = ParseData.parsePatient(result);
                        }else{
                            oUser = ParseData.parseUser(result);
                        }
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
                if(oPatient==null && oUser==null){
                    Toast.makeText(mContext, "User name and password are not matched", Toast.LENGTH_LONG).show();
                }else if(oUser != null){
                    // nurse user logged in
                    Toast.makeText(mContext, "Nurse "+oUser.getUsername()+" logged in", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext , NurseStartActivity.class);
                    intent.putExtra("user", (Serializable) oUser);
                    startActivity(intent);
                    if(bAutoLogin)
                        finish();
                    oUser = null;
                    //finish();
                }else{
                    // patient user logged in
                    Toast.makeText(mContext, "Patient "+oPatient.getUsername()+" logged in", Toast.LENGTH_SHORT).show();
                    Log.w("Patient", "username:"+oPatient.getUsername());
                    Log.w("Patient", "name:"+oPatient.getFirstname()+", "+oPatient.getLastname());
                    Log.w("Patient", "email:"+oPatient.getEmail());
                    Log.w("Patient", "telephone:"+oPatient.getTelephone());

                    String patientId = oPatient.getId();
                    SharedPreferences.Editor editor = app_preferences.edit();
                    editor.remove("patientId");
                    editor.putString("patientId", patientId);
                    editor.apply();
                    editor.commit();

                    Intent intent = new Intent(mContext , PatientMainActivity.class);
                    intent.putExtra("user", (Serializable) oPatient);
                    startActivity(intent);
                    if(bAutoLogin)
                        finish();
                    oPatient = null;
                    //finish();
                }


            }
        }
    }
}

