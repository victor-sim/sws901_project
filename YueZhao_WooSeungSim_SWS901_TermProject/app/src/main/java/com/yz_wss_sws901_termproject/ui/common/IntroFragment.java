package com.yz_wss_sws901_termproject.ui.common;


import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yz_wss_sws901_termproject.R;


/**
 * Created by VictorSim on 14. 10. 26..
 */
public class IntroFragment extends Fragment {

    private String szFrag = "IntroFragment";

    public IntroFragment(){
        Log.d(szFrag, "Default Constructor");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_intro, container, false);
        
        
        getActivity().getActionBar().hide();

        ImageView imgLogo = (ImageView)rootView.findViewById(R.id.logoImage);

        int nOrientation = getActivity().getResources().getConfiguration().orientation;
        Log.d(szFrag, "OnCreateView, rotate? " + nOrientation);
        changeImage(nOrientation, imgLogo);


        imgLogo.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(szFrag, "Touch Image");
                        //startLogin();
                        ((OnTouchListener)getActivity()).startLogInFragment();
                    }
                }
        );

        return rootView;
    }

    public interface OnTouchListener {
        public void startLogInFragment();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(szFrag, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(szFrag, "onCreate");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(szFrag, "onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(szFrag, "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(szFrag, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(szFrag, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(szFrag, "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(szFrag, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(szFrag, "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(szFrag, "onDetach");
    }




    private void changeImage(int orientation, ImageView imgLogo){
        if(orientation == Configuration.ORIENTATION_LANDSCAPE){
            Log.d(szFrag, "Screen Rotated to Landscape");
            imgLogo.setImageResource(R.drawable.homecare);
        } else if (orientation == Configuration.ORIENTATION_PORTRAIT){
            Log.d(szFrag, "Screen Rotated to Portrait");
            imgLogo.setImageResource(R.drawable.homecare);
        }
    }
}

