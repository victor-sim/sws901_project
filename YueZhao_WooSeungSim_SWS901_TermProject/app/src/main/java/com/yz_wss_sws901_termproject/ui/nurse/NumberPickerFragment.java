package com.yz_wss_sws901_termproject.ui.nurse;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.yz_wss_sws901_termproject.ui.nurse.NumberPickerFragment.OnNumberDialogDoneListener} interface
 * to handle interaction events.
 * Use the {@link NumberPickerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NumberPickerFragment extends DialogFragment {

    private NumberPicker[] numPickers;
    private int numDials;
    private int currentValue;
    private int tv;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_numDials = "numDials";
    private static final String ARG_initValue = "initValue";
    private static final String ARG_viewID="viewID";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnNumberDialogDoneListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NumberPickerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NumberPickerFragment newInstance(int numDials, int initValue,int viewID) {
        NumberPickerFragment fragment = new NumberPickerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_numDials, numDials);
        args.putInt(ARG_initValue, initValue);
        args.putInt(ARG_viewID,viewID);
        fragment.setArguments(args);
        return fragment;
    }

    public NumberPickerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            numDials = getArguments().getInt(ARG_numDials);
            currentValue = getArguments().getInt(ARG_initValue);
            tv=getArguments().getInt(ARG_viewID);
            numPickers = new NumberPicker[numDials];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //NumberPickers Layout
        LinearLayout linearLayoutH = new LinearLayout(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayoutH.setLayoutParams(params);

        for (int i = 0; i < numDials; i++) {
            numPickers[numDials - i - 1] =
                    new NumberPicker(getActivity());
            numPickers[numDials - i - 1].setMaxValue(9);
            numPickers[numDials - i - 1].setMinValue(0);
            numPickers[numDials - i - 1].
                    setValue(getDigit(currentValue, numDials - i - 1));
            linearLayoutH.addView(numPickers[numDials - i - 1]);
        }

        //Fragment Layout
        LinearLayout linLayoutV =
                new LinearLayout(getActivity());
        linLayoutV.setOrientation(LinearLayout.VERTICAL);
        linLayoutV.addView(linearLayoutH);

        Button okButton = new Button(getActivity());
        okButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        currentValue = getValue();
                        if (mListener != null) {
                            mListener.onDone(currentValue,tv);
                        }
                        dismiss();
                    }
                });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        okButton.setLayoutParams(params);
        okButton.setText("Done");

        linLayoutV.addView(okButton);
        return linLayoutV;

    }


    @Override
    public void onAttach(Activity activity) {


        super.onAttach(activity);
        try {
            mListener = (OnNumberDialogDoneListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnNumberDialogDoneListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnNumberDialogDoneListener {
        public void onDone(int value,int viewID);
    }

    private int getDigit(int d, int i) {
        String temp = Integer.toString(d);
        if (temp.length() <= i) return 0;
        int r = Character.getNumericValue(
                temp.charAt(temp.length() - i - 1));
        return r;
    }

    private int getValue() {
        int value = 0;
        int mult = 1;
        for (int i = 0; i < numDials; i++) {
            value += numPickers[i].getValue() * mult;
            mult *= 10;
        }
        return value;
    }
}
