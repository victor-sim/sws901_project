package com.yz_wss_sws901_termproject.ui.nurse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.History;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.ui.common.WelcomeActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public class PatientHistoryActivity extends Activity {
    private ArrayList<History> arrHistory;
    private BaseAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_history);
        final Patient patient = getIntent().getParcelableExtra("patient");
        TextView tv_pn = (TextView) findViewById(R.id.tv_history_list_pn);
        tv_pn.setText(patient.getFirstname() + " " + patient.getLastname());
        tv_pn.setTypeface(null, Typeface.BOLD_ITALIC);
        new GetData(this, patient).execute("");

        ListView lv_history = (ListView) findViewById(R.id.lv_history);
        mAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                if (arrHistory == null)
                    return 0;
                return arrHistory.size();
            }

            @Override
            public Object getItem(int position) {
                return arrHistory.get(position);
            }

            @Override
            public long getItemId(int position) {
/*                long nId;
                nId = Long.parseLong(((History) getItem(position)).getId());
                return nId;*/
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (arrHistory == null) {
                    return convertView;
                }
                final View view = convertView.inflate(PatientHistoryActivity.this, R.layout.historylist_slot, null);

                History oHistory = (History) getItem(position);

                final TextView tv_BT = (TextView) view.findViewById(R.id.tv_history_slot_BT);
                final TextView tv_HR = (TextView) view.findViewById(R.id.tv_history_slot_HR);
                final TextView tv_sys = (TextView) view.findViewById(R.id.tv_history_slot_systolic);
                final TextView tv_dias = (TextView) view.findViewById(R.id.tv_history_slot_diastolic);
                final TextView tv_resp = (TextView) view.findViewById(R.id.tv_history_slot_resp);
                final TextView tv_date = (TextView) view.findViewById(R.id.tv_history_slot_date);

                tv_BT.setText(oHistory.getBodyTemp()+"  ");
                tv_BT.setTypeface(null,Typeface.BOLD);
                tv_HR.setText(oHistory.getHeartrate());
                tv_HR.setTypeface(null,Typeface.BOLD);
                tv_sys.setText(oHistory.getSystolic());
                tv_sys.setTypeface(null,Typeface.BOLD);
                tv_dias.setText(oHistory.getDiastolic());
                tv_dias.setTypeface(null,Typeface.BOLD);
                tv_resp.setText(oHistory.getRespiratory());
                tv_resp.setTypeface(null,Typeface.BOLD);
                tv_date.setText(oHistory.getRecDate());
                tv_date.setTypeface(null,Typeface.BOLD);

                return view;
            }
        };
        lv_history.setAdapter(mAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.putExtra("action", "logout");
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loginIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private int mType;
        private DialogActivity myDialog;
        private Patient patient;

        private GetData(Context context, Patient patient) {
            this.mContext = context;
            myDialog = new DialogActivity(context);
            this.patient = patient;
            //this.mType = type;
        }

        @Override
        protected void onPreExecute() {
            if (mType == 0) {
                ;
            }
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(
                    PatientHistoryActivity.this);

            try {
                result = request.getHistory(Integer.parseInt(patient.getId()));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {
                Toast.makeText(mContext, "no result", Toast.LENGTH_LONG).show();
            } else {

                arrHistory = null;
                try {
                    arrHistory = new ParseData().getHistoryList(result);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
                if (arrHistory != null) {
                    /*Intent intent = new Intent(PatientListActivity.this,
                            PatientListActivity.class);
					intent.putExtra("patients", (Serializable) arrPatient);
					startActivity(intent);
					PatientListActivity.this.finish();*/
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(mContext, "History List updated", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Some error on history list", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}
