package com.yz_wss_sws901_termproject.ui.common;

import java.io.InputStream;

import com.yz_wss_sws901_termproject.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class LoadingView extends View{
	
	private Movie mMovie = null;
	private long movieStart = 0;
	private int drawableId = R.drawable.loading_gif;
	
	private void initView(){
		InputStream inStream = getContext().getResources().openRawResource(drawableId);
		mMovie = Movie.decodeStream(inStream);
		movieStart = 0;
	}
	
	public LoadingView(Context context){
		super(context);
		drawableId = R.drawable.loading_gif;
		initView();
	}

	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		drawableId = R.drawable.loading_gif;
		initView();
	}
	
	public LoadingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		drawableId = R.drawable.loading_gif;
		initView();
    }
	
	@Override
	protected void onDraw(Canvas canvas){
		canvas.drawColor(Color.TRANSPARENT);
		super.onDraw(canvas);
		long now = SystemClock.uptimeMillis();
        if (movieStart == 0) {
            movieStart = now;
        }
        if (mMovie != null) {
            int relTime = (int) ((now - movieStart) % mMovie.duration());
            mMovie.setTime(relTime);
            float fScale = getWidth() / mMovie.width();
            //Log.e("Befor scale Canvas W and H", "W: "+canvas.getWidth()+ " H:"+canvas.getHeight());
            canvas.scale(fScale, fScale);
            //Log.e("W and H", "W: "+getWidth()+ " H:"+getHeight());
            //Log.e("Canvas W and H", "W: "+canvas.getWidth()+ " H:"+canvas.getHeight());
            //Log.e("Movie W and H", "W: "+mMovie.width()+ " H:"+mMovie.height());
            mMovie.draw(canvas, (getWidth()/fScale - mMovie.width())/2, (getHeight()/fScale - mMovie.height())/2);
            this.invalidate();
        }
	}

}
