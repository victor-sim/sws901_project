package com.yz_wss_sws901_termproject.ui.nurse;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonSyntaxException;
import com.yz_wss_sws901_termproject.R;
import com.yz_wss_sws901_termproject.beans.Location;
import com.yz_wss_sws901_termproject.beans.Patient;
import com.yz_wss_sws901_termproject.tools.GPSTracker;
import com.yz_wss_sws901_termproject.ui.common.DialogActivity;
import com.yz_wss_sws901_termproject.web.ParseData;
import com.yz_wss_sws901_termproject.web.WebRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public class PatientMapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Patient patient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_maps);

        patient=getIntent().getParcelableExtra("patient");


        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                new GetData(this,patient.getId()).execute("");
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(43, 79)).title("Marker"));
    }

    private class GetData extends AsyncTask<String, String, String> {
        private Context mContext;
        private DialogActivity myDialog;
        private String patientId;

        private GetData(Context context, String patientId) {
            this.mContext = context;
            this.patientId = patientId;
            myDialog = new DialogActivity(context);
        }

        @Override
        protected void onPreExecute() {
            myDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            WebRequest request = new WebRequest(mContext);

            try {
                result = request.getLocationList(patientId);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            myDialog.dismiss();
            if (result == null || result.equals("")) {
                Toast.makeText(mContext, "no location history", Toast.LENGTH_LONG).show();
            } else {
                Log.e("onPostExecute", result);
                try {
                    ArrayList<Location> arrLocs = ParseData.getLocationList(result);

                    if(arrLocs != null){
                        Location prevLoc = null;
                        float hue = BitmapDescriptorFactory.HUE_ROSE;
                        for(Location aLoc : arrLocs){
                            Log.e("Locs", "Lat:"+aLoc.getLatitude()+", Log"+aLoc.getLongitude());
                            MarkerOptions marker =
                                    new MarkerOptions()
                                            .position(new LatLng(aLoc.getLatitude(), aLoc.getLongitude()))
                                            .title(patient.getFirstname() + ", " + patient.getLastname())
                                            .snippet(aLoc.getRecdate());
                            marker.icon(BitmapDescriptorFactory.defaultMarker(hue));
                            hue -= 20;
                            mMap.addMarker(marker);
                            if(prevLoc != null){
                                mMap.addPolyline(
                                        new PolylineOptions()
                                                .add(new LatLng(aLoc.getLatitude(), aLoc.getLongitude())
                                                        , new LatLng(prevLoc.getLatitude(), prevLoc.getLongitude()))
                                                .width(5)
                                                .color(getResources().getColor(R.color.wallet_secondary_text_holo_dark))
                                                .geodesic(true)) ;
                            }

                            prevLoc = aLoc;
                        }
                        if(arrLocs.size() <= 0){
                            GPSTracker gpsTracker = new GPSTracker(mContext);
                            android.location.Location loc = gpsTracker.getLocation();
                            prevLoc = new Location();
                            prevLoc.setLongitude(loc.getLongitude());
                            prevLoc.setLatitude(loc.getLatitude());
                            Toast.makeText(mContext, "There are no location history", Toast.LENGTH_LONG).show();
                        }else{
                            prevLoc = arrLocs.get(0);
                        }

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(prevLoc.getLatitude(),prevLoc.getLongitude()),18));
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
