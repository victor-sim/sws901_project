package com.yz_wss_sws901_termproject.web;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.yz_wss_sws901_termproject.beans.*;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;


public class WebRequest {

	
	private final String url = "http://victor-sim.ddns.net/sws901/";

	protected Context mContext;

	private BaseRequest baseRequest;

	public WebRequest(Context context) {
		baseRequest = new BaseRequest();
		mContext = context;
	}

	public String getUrl(String u, String a) {
		StringBuilder sb = new StringBuilder();
		sb.append(url).append(u).append("/").append(a);
		return sb.toString();
	}
	
	
	public String login(String userName, String password) throws IOException, TimeoutException {
		ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
		if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password) ) {
			strParams.add(new BasicNameValuePair("username", userName));
			strParams.add(new BasicNameValuePair("password", password));
		}
		else{
			return null;
		}
		return baseRequest.postRequestByHttpClient(strParams, getUrl("login.php", ""));
	}
	
	public String getPatientList() throws IOException, TimeoutException {
		ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
		return baseRequest.postRequestByHttpClient(strParams,
				getUrl("getpatients.php", ""));
	}
	
	public String addPatient(Patient oPatient) 
			throws IOException, TimeoutException {
		ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
		if (oPatient != null ) {
			strParams.add(new BasicNameValuePair("username", oPatient.getUsername()));
			strParams.add(new BasicNameValuePair("password", oPatient.getPassword()));
			strParams.add(new BasicNameValuePair("firstname", oPatient.getFirstname()));
			strParams.add(new BasicNameValuePair("lastname", oPatient.getLastname()));
			strParams.add(new BasicNameValuePair("telephone", oPatient.getTelephone()));
			strParams.add(new BasicNameValuePair("email", oPatient.getEmail()));
			strParams.add(new BasicNameValuePair("address", oPatient.getAddress()));
			strParams.add(new BasicNameValuePair("emer_phone", oPatient.getEmer_phone()));
			strParams.add(new BasicNameValuePair("emer_email", oPatient.getEmer_email()));
		}
		else{
			return null;
		}
		return baseRequest.postRequestByHttpClient(strParams, getUrl("addpatient.php", ""));
	}
	
	public String addNurse(User oUser) 
			throws IOException, TimeoutException {
		ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
		if (oUser != null ) {
			strParams.add(new BasicNameValuePair("username", oUser.getUsername()));
			strParams.add(new BasicNameValuePair("password", oUser.getPassword()));
			strParams.add(new BasicNameValuePair("firstname", oUser.getFirstname()));
			strParams.add(new BasicNameValuePair("lastname", oUser.getLastname()));
			strParams.add(new BasicNameValuePair("telephone", oUser.getTelephone()));
			strParams.add(new BasicNameValuePair("email", oUser.getEmail()));
			strParams.add(new BasicNameValuePair("address", oUser.getAddress()));
		}
		else{
			return null;
		}
		return baseRequest.postRequestByHttpClient(strParams, getUrl("addnurse.php", ""));
	}
	
	public String addHistory(History oHistory) 
			throws IOException, TimeoutException {
		ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
		if (oHistory != null ) {
			strParams.add(new BasicNameValuePair("patientid", oHistory.getPatientId() ) );
			strParams.add(new BasicNameValuePair("bodytemperature", oHistory.getBodyTemp() ) );
			strParams.add(new BasicNameValuePair("heartrate", oHistory.getHeartrate() ) );
			strParams.add(new BasicNameValuePair("systolic", oHistory.getSystolic() ) );
			strParams.add(new BasicNameValuePair("diastolic", oHistory.getDiastolic() ) );
			strParams.add(new BasicNameValuePair("respiratory", oHistory.getRespiratory() ) );
			
		}
		else{
			return null;
		}
		return baseRequest.postRequestByHttpClient(strParams, getUrl("addhistory.php", ""));
	}
	
	public String getHistory(int patientid) throws IOException, TimeoutException {
		ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
		strParams.add(new BasicNameValuePair("patientid", Integer.toString(patientid) ) );
		return baseRequest.postRequestByHttpClient(strParams,
				getUrl("gethistory.php", ""));
	}

    public String getRecommend(int patientid) throws IOException, TimeoutException {
        ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
        strParams.add(new BasicNameValuePair("patientid", Integer.toString(patientid) ) );
        Log.e("getRecommend", "strParams " + strParams);
        return baseRequest.postRequestByHttpClient(strParams,
                getUrl("getRecommendation.php", ""));
    }

    public String addRecommend(Recommend oRecommend)
            throws IOException, TimeoutException {
        ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
        if (oRecommend != null ) {
            strParams.add(new BasicNameValuePair("patientid", oRecommend.getPatientId() ) );
            strParams.add(new BasicNameValuePair("message", oRecommend.getMessage() ) );
            strParams.add(new BasicNameValuePair("iswrist", Boolean.toString(oRecommend.getIsWrist()) ) );
            strParams.add(new BasicNameValuePair("level", Integer.toString(oRecommend.getLevel()) ) );
            strParams.add(new BasicNameValuePair("duration", Integer.toString(oRecommend.getDuration()) ) );
            strParams.add(new BasicNameValuePair("duedate", oRecommend.getDueDate() ) );

        }
        else{
            return null;
        }
        return baseRequest.postRequestByHttpClient(strParams, getUrl("addRecommendation.php", ""));
    }

    public String addLocation(String patientId, Double longitude, Double latitude)throws IOException, TimeoutException {
        ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
        strParams.add(new BasicNameValuePair("patientid", patientId ) );
        strParams.add(new BasicNameValuePair("longitude", Double.toString(longitude) ) );
        strParams.add(new BasicNameValuePair("latitude", Double.toString(latitude) ) );

        return baseRequest.postRequestByHttpClient(strParams, getUrl("addLocation.php", ""));

    }

    public String getLocationList(String patientId) throws IOException, TimeoutException {
        ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
        strParams.add(new BasicNameValuePair("patientid", patientId ) );
        return baseRequest.postRequestByHttpClient(strParams,
                getUrl("getLocation.php", ""));
    }

    public String getLocationList(String patientId, int limit) throws IOException, TimeoutException {
        ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
        strParams.add(new BasicNameValuePair("patientid", patientId ) );
        strParams.add(new BasicNameValuePair("limit", Integer.toString(limit) ) );
        return baseRequest.postRequestByHttpClient(strParams,
                getUrl("getLocation.php", ""));
    }

    public String updatePatientInfo(Patient oPatient) throws IOException, TimeoutException{
        ArrayList<NameValuePair> strParams = new ArrayList<NameValuePair>();
        strParams.add(new BasicNameValuePair("id", oPatient.getId() ) );
        strParams.add(new BasicNameValuePair("lastname", oPatient.getLastname() ) );
        strParams.add(new BasicNameValuePair("firstname", oPatient.getFirstname() ) );
        strParams.add(new BasicNameValuePair("telephone", oPatient.getTelephone() ) );
        strParams.add(new BasicNameValuePair("email", oPatient.getEmail() ) );
        strParams.add(new BasicNameValuePair("address", oPatient.getAddress() ) );
        strParams.add(new BasicNameValuePair("emergency_phone", oPatient.getEmer_phone() ) );
        strParams.add(new BasicNameValuePair("emergency_email", oPatient.getEmer_email() ) );
        return baseRequest.postRequestByHttpClient(strParams,
                getUrl("updatePatient.php", ""));
    }
	
}
