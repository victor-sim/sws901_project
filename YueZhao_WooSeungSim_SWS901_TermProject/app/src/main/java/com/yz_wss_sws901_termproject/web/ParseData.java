package com.yz_wss_sws901_termproject.web;


import java.lang.reflect.Type;
import java.util.ArrayList;

import android.util.Log;

import com.yz_wss_sws901_termproject.beans.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public class ParseData {


	static public CommonResult CommonPares(String str) {
		Gson gson = new Gson();
		Type type = new TypeToken<CommonResult>() {
		}.getType();
		CommonResult model = new CommonResult();
		model = gson.fromJson(str, type);
		return model;
	}
	
	static public boolean isLoggedIn(String str) {
		
		if(str == null){
			return false;
		}else if(str.equals("null")){
			return false;
		}
		return true;
	}
	
	static public boolean isPatient(String str){
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		User oUser = gson.fromJson(parser.parse(str), User.class);
		if(oUser.isPatient()){
			return true;
		}
		return false;
	}
	
	static public User parseUser(String str) throws JsonSyntaxException {
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		User oUser = gson.fromJson(parser.parse(str), User.class);

		return oUser;
	}
	
	static public Patient parsePatient(String str) throws JsonSyntaxException {
		Gson gson = new Gson();
		Log.d("parsePatient", str);
		JsonParser parser = new JsonParser();
		Patient oPatient = gson.fromJson(parser.parse(str), Patient.class);
        Log.e("parsed", oPatient.toString());

		return oPatient;
	}

    static public Recommend parseRecommend(String str) throws JsonSyntaxException{
        Gson gson = new Gson();
        Log.d("parseRecommend", str);
        JsonParser parser = new JsonParser();
        Recommend oRecommend = gson.fromJson(parser.parse(str), Recommend.class);
        Log.e("parsed", oRecommend.toString());

        return oRecommend;
    }
	
	public ArrayList<History> getHistoryList(String str) throws JsonSyntaxException{
		Gson gson = new Gson(); 
	    JsonParser parser = new JsonParser(); 
	    JsonArray Jarray = parser.parse(str).getAsJsonArray(); 
	    ArrayList<History> lcs = new ArrayList<History>(); 
	    for(JsonElement obj : Jarray ){ 
	    	History cse = gson.fromJson( obj , History.class); 
	        lcs.add(cse); 
	    }

		return lcs;
	}
	
	public ArrayList<Patient> getPatientList(String str) throws JsonSyntaxException{
		Gson gson = new Gson(); 
	    JsonParser parser = new JsonParser(); 
	    Log.e("getPatientList", "out: "+ str);
	    JsonArray Jarray = parser.parse(str).getAsJsonArray(); 
	    ArrayList<Patient> lcs = new ArrayList<Patient>(); 
	    for(JsonElement obj : Jarray ){ 
	    	Patient cse = gson.fromJson( obj , Patient.class); 
	        lcs.add(cse); 
	    }

		return lcs;
	}

    static public ArrayList<Location> getLocationList(String str) throws JsonSyntaxException{
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        Log.e("getLocationList", "out: "+ str);
        JsonArray Jarray = parser.parse(str).getAsJsonArray();
        ArrayList<Location> lcs = new ArrayList<Location>();
        for(JsonElement obj : Jarray ){
            Location cse = gson.fromJson( obj , Location.class);
            lcs.add(cse);
        }

        return lcs;
    }


	
	
}
